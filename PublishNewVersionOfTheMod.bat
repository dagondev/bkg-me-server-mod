
call "C:\Users\nao\Documents\Windward\Mods\BkgMe Server Mod Development\MoveNewVersionOfTheMod.bat"

set SETUP_NAME=BkgMe Server Mod.exe
set LOCALIZATION_NAME=Localization.csv
set CONFIG_NAME=Config.txt
set VERSION_NAME=Version.txt
set CHANGELOG_NAME=changelog.txt
set INSTALL_SCRIPT_NAME=install.sh

set DEV_MOD_PATH=C:\Users\nao\Documents\Windward\Mods\BkgMe Server Mod Development
set MOD_PATH=C:\Users\nao\Documents\Windward\Mods\BkgMe Server Mod
set SETUP_PATH=C:\Users\nao\Documents\Windward\Mods\BkgMe Server Mod Development\BkgMe Server Mod Development\Output
set DROPBOX_PATH=D:\Dropbox\Public\BkgMe\static\server-mod
set ZIP_NAME=BkgMe Server Mod.zip
set TAR_NAME=BkgMe Server Mod.tar
set TAR_GZ_NAME=BkgMe Server Mod.tgz
set INSTALLER_FILE1_NAME=unins000.exe
set INSTALLER_FILE2_NAME=unins000.dat
set STEAM_SHORTCUT=RunWindward.url

set /p VERSION=<"%MOD_PATH%\%VERSION_NAME%"
set ZIP_VERSION_NAME=%VERSION%.zip
set 7Z_PATH=C:\Users\nao\Documents\Windward\Mods\BkgMe Server Mod Development\7z.exe
set 7ZA_PATH=C:\Users\nao\Documents\Windward\Mods\BkgMe Server Mod Development\7za.exe

echo Deleting installer files
if exist { %MOD_PATH%\%INSTALLER_FILE1_NAME% } (
	echo Removing %INSTALLER_FILE1_NAME%
	rm "%MOD_PATH%\%INSTALLER_FILE1_NAME%"
)
if exist { %MOD_PATH%\%INSTALLER_FILE2_NAME% } (
	echo Removing %INSTALLER_FILE2_NAME%
	rm "%MOD_PATH%\%INSTALLER_FILE2_NAME%"
)

REM http://superuser.com/questions/110991/can-you-zip-a-file-from-the-command-prompt-using-only-windows-built-in-capabili
echo Creating zip file
cd "%DEV_MOD_PATH%"
7z.exe a "%DROPBOX_PATH%\%ZIP_NAME%" "%MOD_PATH%"

echo Creating tar.gz file
if exist { %DEV_MOD_PATH%\%TAR_GZ_NAME% } (
	echo Removing old %DEV_MOD_PATH%\%TAR_GZ_NAME%
	rm "%DEV_MOD_PATH%\%TAR_GZ_NAME%"
)

cd "%DEV_MOD_PATH%"
7za a -ttar -so "%TAR_NAME%" "%MOD_PATH%" | 7za.exe a -si "%TAR_GZ_NAME%"
cp "%TAR_GZ_NAME%" "%DROPBOX_PATH%\%TAR_GZ_NAME%"

echo Uploading new version: %VERSION% to Dropbox
echo Creating backup to the old-version section
cp "%DROPBOX_PATH%\%ZIP_NAME%" "%DROPBOX_PATH%\old-versions\%ZIP_VERSION_NAME%"
echo Copying individual files
cp "%DEV_MOD_PATH%\%INSTALL_SCRIPT_NAME%" "%DROPBOX_PATH%\%INSTALL_SCRIPT_NAME%"
cp "%MOD_PATH%\%VERSION_NAME%" "%DROPBOX_PATH%\%VERSION_NAME%"
cp "%MOD_PATH%\%CHANGELOG_NAME%" "%DROPBOX_PATH%\%CHANGELOG_NAME%"
cp "%SETUP_PATH%\%SETUP_NAME%" "%DROPBOX_PATH%\%SETUP_NAME%"
echo Uploaded new version: %VERSION% to Dropbox

PAUSE