@echo off

set DEV_MOD_PATH=C:\Users\nao\Documents\Windward\Mods\BkgMe Server Mod Development
set MOD_PATH=C:\Users\nao\Documents\Windward\Mods\BkgMe Server Mod
set DEV_DLL_NAME=BkgMe Server Mod Development.dll
set DLL_NAME=BkgMe Server Mod.dll
set LOCALIZATION_NAME=Localization.csv
set CONFIG_NAME=Config.txt
set VERSION_NAME=Version.txt
set CHANGELOG_NAME=changelog.txt
set INSTALL_SCRIPT_NAME=InstallNewVersion.bat
set DDGUI_PATH=C:\Users\nao\Documents\Windward\Mods\DDGUIExample
set DDGUI_NAME=DDGUIExample.dll
set STEAM_SHORTCUT=RunWindward.url

IF EXIST "%MOD_PATH%" (
echo Removing folder: %MOD_PATH% 
    rmdir "%MOD_PATH%" /s /q
)
echo Creating folder: %MOD_PATH%
mkdir "%MOD_PATH%"

cp "%DEV_MOD_PATH%\BkgMe Server Mod Development\bin\Release\%DEV_DLL_NAME%" "%MOD_PATH%\%DLL_NAME%"
cp "%DEV_MOD_PATH%\%LOCALIZATION_NAME%" "%MOD_PATH%\%LOCALIZATION_NAME%"
cp "%DEV_MOD_PATH%\%CONFIG_NAME%" "%MOD_PATH%\%CONFIG_NAME%"
cp "%DEV_MOD_PATH%\%VERSION_NAME%" "%MOD_PATH%\%VERSION_NAME%"
cp "%DEV_MOD_PATH%\%CHANGELOG_NAME%" "%MOD_PATH%\%CHANGELOG_NAME%"
cp "%DDGUI_PATH%\%DDGUI_NAME%" "%MOD_PATH%\%DDGUI_NAME%"
cp "%DEV_MOD_PATH%\%STEAM_SHORTCUT%" "%MOD_PATH%\%STEAM_SHORTCUT%"