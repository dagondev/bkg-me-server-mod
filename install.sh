#!/bin/bash
mod_folder_name="BkgMe Server Mod"
update_tgz_name="BkgMe Den Server Mod.tgz"
windward_path="$HOME/Documents/Windward"
bkg_me_path_to_tgz="http://dagonden.ddns.net/static/server-mod/Dagon%20Den%20Server%20Mod.tgz"
echo "BkgMe Server Mod installation script v0.11"
echo "================"
if [ $# -eq 0 ]
  then
    echo "No arguments supplied."
	echo "If you want to define where windward player files are, run this script with your path passed as argument. i.e.: /install.sh $windward_path"
else
	windward_path=$0
fi
mods_path="$windward_path/Mods"
dds_mod_path="$mods_path/$mod_folder_name"
echo "Assuming path $windward_path for Windward player files."
echo "This script is going to extract $update_tgz_name (and download it first if there is none), delete whole folder $dds_mod_path (if it exists) and copy extracted data to $mods_path (and create Mods folder if needed)." 
echo "================"
read -t 10 -p "Hit ENTER or wait ten seconds to start updating."
echo ""
echo "================"
if [ ! -e "$update_tgz_name" ]
then
    echo "There is no $update_tgz_name here. Downloading one from Bkg.Me Website..."
	wget "$bkg_me_path_to_tgz"
fi
echo "Unpacking $update_tgz_name:"
tar zxvf "$update_tgz_name"
if [ -e "$dds_mod_path" ]
then
	echo "$dds_mod_path exists, deleting it first."
	rm -rf "$dds_mod_path"
elif [ ! -e "$mods_path" ]
then
	echo "There is no Mods folder in $windward_path, creating it."
	mkdir -p "$mods_path"
fi
echo "moving $mod_folder_name to $mods_path"
mv "$mod_folder_name" "$mods_path/"
echo "Done."
echo "Deleting tgz file..."
rm "$update_tgz_name"
echo "Running Windward via steam..."
steam steam://rungameid/326410