Version
	Quests
		QuestTransportGoodsToLighthouse
			minSize = 4
			reward = 5
			xp = 15
			reputation = 50
			chance = 0.25
			icon = "Item - Crates"
		QuestSupplyRepairsOfTowers
			reward = 5
			xp = 25
			icon = "Item - Scroll"
			reputation = 250
			chance = 0.10
			minSize = 10
			Attach = string[]
				SkillPassRepairSupplies