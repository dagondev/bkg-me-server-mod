﻿using UnityEngine;
using DDGUI;
using System;


class GUIManager
{
    public static void Init(bool isWindows)
    {
        GameObject parent = DDGUIHelper.GetMainMenuGUIParentForNewElements();
        GUIMainMenuRightPanel.Init(parent, MainMenuManager.GetEventDelegateHandlePanelClosing(),MainMenuManager.GetEventDelegateHandleUpdatingClient(),isWindows);
    }
    public static void OnChangingRegions()
    {
        GameObject parent = DDGUIHelper.GetGameGUIParentForNewElements();
        GUIFactionChange.Initialize(parent);
        GUIServerPanel.Init(parent,new EventDelegate(()=>ServerPanelManager.HandleIconClick()));
        foreach (GUIServerPanel.SettingType type in GUIServerPanel.SettingType.GetValues(typeof(GUIServerPanel.SettingType)))
        {
            ServerPanelManager serverPanelManager = new ServerPanelManager(type);
            GUIServerPanel.AddChangeSettingEventDelegate(type, new EventDelegate(() => { serverPanelManager.HandleSettingToggle(); }));
        }
        GUIServerAdminTools.Init(parent);  
            
    }
}
