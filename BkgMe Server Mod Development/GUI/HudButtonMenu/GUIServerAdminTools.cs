﻿using System.Collections;
using UnityEngine;
using DDGUI;

class GUIServerAdminTools
{
    public static UIWidget Icon;
    static UIInput currentRegionInput;
    static UIInput currentFactionInput;
    static UILabel gameSunLabel;
    public static UILabel lastTimeUpdated;
    static UIPanel toolsWindow;
    public static void Init(GameObject parent)
    {
        if (parent != null)
            CreateMenuIcon(parent);
    }
    public static void UpdateLastTimeUpdateLabel(string text)
    {
        if (GUIServerAdminTools.lastTimeUpdated != null)
            lastTimeUpdated.text = text;
    }
    public static void ShowWindow()
    {
        if (toolsWindow == null)
            CreateToolsWindow();
        else
            UIWindow.Show(toolsWindow);
    }
    protected static void CreateToolsWindow()
    {

        GameObject parent = DDGUIHelper.GetGameGUIParentForNewElements();
        DDGUIAdvancedWidgetsCreator windowCreator = new DDGUIAdvancedWidgetsCreator();
        int width = windowCreator.ChildrenWidth;
        int height = 50;

        toolsWindow = windowCreator.CreateBasicWindow(parent, "Admin Tools", "");
        lastTimeUpdated = windowCreator.AddLabel("Last updated: " + System.DateTime.Now.ToLongTimeString());
        windowCreator.AddLabel(width, height, "Selected player: " + MyPlayer.ship.name);
        windowCreator.AddLabel(width, height, "Set current region:");
        currentRegionInput = windowCreator.AddNumericInput();
        currentRegionInput.value = "" + MyPlayer.zoneID;
        windowCreator.AddBigButton(width, height, new EventDelegate(() => { MyPlayer.saveNeeded = true; MyPlayer.TravelTo(int.Parse(currentRegionInput.value)); }), "Teleport");

        windowCreator.AddLabel(width, height, "Set current faction:");
        currentFactionInput = windowCreator.AddNumericInput();
        currentFactionInput.value = "" + MyPlayer.factionID;
        windowCreator.AddBigButton(width, height, new EventDelegate(() => { MyPlayer.factionID = int.Parse(currentFactionInput.value); MyPlayer.saveNeeded = true; UIStatusBar.Show("Done"); }), "Change faction");
        windowCreator.AddBigButton(width, height, new EventDelegate(() => { ServerAdminTools.HandleFactionRegionConquering(); }), "Set everything (except other players) as belongings to your faction");
        windowCreator.AddBigButton(width, height, new EventDelegate(() => { MyPlayerCustomValues.WasSeenOnServer = false; MyPlayerCustomValues.HasturQuest = false; UIStatusBar.Show("Done"); }), "Reset was seen on server custom value");
        windowCreator.AddBigButton(width, height, new EventDelegate(() => { ServerAdminTools.HandleDiscoveringEverything(); }), "Discover everything");
        windowCreator.AddSmallDivider();
        //windowCreator.AddLabel("Lists of players that have quests in progress: ("+Quest.list.Count+")");
        //UILabel questsLabel = windowCreator.AddLabel("");
        //foreach (Quest quest in Quest.list)
        //    questsLabel.text += quest.name + "\n";

        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            GameTown town = GameTown.FindClosest(MyPlayer.ship.position);
            town.AddQuests(5);
            UIStatusBar.Show("Done");
        }), "Generate 5 quests for nearest town");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            GameTown town = GameTown.FindClosest(MyPlayer.ship.position);
            int n = town.quests.Count;
            town.quests.Clear();
            town.AddQuests(n);
            UIStatusBar.Show("Done");
        }), "Reset quest in the nearest town");
        windowCreator.AddSmallDivider();
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            DDGUIDebug.DebugOn = true;
            DDGUIDebug.PrintDataNodeObjectToFile(GameConfig.data);
            //DDGUIDebug.PrintDataNodeObjectToFile(GameConfig.factions);
            DDGUIDebug.PrintDataNodeObjectToFile(MyPlayer.data);
            //DDGUIDebug.PrintDataNodeObjectToFile(MyPlayer.worldData);
            //DDGUIDebug.PrintDataNodeObjectToFile(TNManager.player.dataNode);
            DDGUIDebug.PrintDataNodeObjectToFile(MyPlayer.inventory);
            foreach (GameTown.QuestEntry questEntry in GameTown.FindClosest(MyPlayer.ship.position).quests)
                DDGUIDebug.PrintDataNodeObjectToFile(questEntry.data);
            UIStatusBar.Show("Done");
            DDGUIDebug.DebugOn = false;
        }), "Dump DataNodes");
        gameSunLabel = windowCreator.AddLabel("");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            GameSun.color = Color.black;
            UIStatusBar.Show("Done");
        }), "Set color");
        windowCreator.AddSmallDivider();
        windowCreator.AddLabel("Events!");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.Generate10QuestForEachTownEvent();
        }), "Generate 10 quests for all of the towns");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.TwiceCurrentlyFloatingLootEvent();
        }), "Twice the floating loot (random new loot) in the region");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.ResetAllTownsEvent();
        }), "Reset stuff in towns");

        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.GenerateSupplyOfRandomResourceInAllTownsEvent();
        }), "Add stock of random resource to all towns in the region");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.GenerateShortageOfRandomResourceInAllTownsEvent();
        }), "Make demand resource to all towns in the region");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.GeneratePlayerFactionFleet();
        }), "Spawn 5 ships of ruling faction in the region");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.GeneratePirateFleetEvent();
        }), "Spawn 5 pirate ships in the region");
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.GeneratePirateLordWithFleetEvent();
        }), "Spawn Pirate Lord with 5 ships in the region");
        UIWindow.Show(toolsWindow);
        windowCreator.AddBigButton(width, height, new EventDelegate(() =>
        {
            KnownEvents.broadcastMessage = true;
            KnownEvents.FireRandomKnownEvent();
        }), "Fire random event");

        UIWindow.Show(toolsWindow);
    }

    protected static void CreateMenuIcon(GameObject parent)
    {
        UIFont font = DDGUIResources.GetMediumUIFont();
        UIAtlas atlas = DDGUIResources.GetWindwardUIAtlas();
        if (GUIServerPanel.Icon == null)
            return;
        GameObject playersHudButtonGameObject = GameObject.Find("HUD Button - Who All");
        //check: http://stackoverflow.com/a/25683073
        //Button is the name of the sprite in the atlas
        UIWidgetContainer widgetContainer = NGUITools.AddChild<UIWidgetContainer>(parent);
        GameObject prefab = DDGUIResources.GetHUDButtonPrefab();
        GameObject prefabChild = NGUITools.AddChild(parent, prefab);
        prefabChild.GetComponentInChildren<UILocalize>().key = "DDS AdminTools IconText";

        prefabChild.GetComponentInChildren<UISprite>().spriteName = "Item - Ammo01";
        UIWidget background = prefabChild.GetComponent<UIWidget>();
        background.SetAnchor(playersHudButtonGameObject, 0, -185, 35, -175);
        prefabChild.GetComponent<UIButton>().onClick.Add(new EventDelegate(() => { ServerAdminTools.HandleAdminToolsShowRequest(); }));
        Icon = background;
    }
}

