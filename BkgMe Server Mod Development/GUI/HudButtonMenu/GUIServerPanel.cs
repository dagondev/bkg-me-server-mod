﻿using UnityEngine;
using Tasharen;
using DDGUI;
using System.Collections.Generic;

class GUIServerPanel
{
    public static UIWidget Icon;
    static UIPanel serverWindow;
    static UILabel statsLabel;

    public enum SettingType
    { 
        CommentingShipWreck,
        CommentingMidnightAndNewDay,
        CommeningNewWeekMonthYear,
        EventsHappening
    }
    static Dictionary<SettingType, UIButton> settingButtonDictionary;
    public static string PlayerStats
    {
        get { return statsLabel.text; }
        set { statsLabel.text = value; }
    }
    public static void Init(GameObject parent, EventDelegate iconClick)
    {
        if (parent != null)
        {
            settingButtonDictionary = new Dictionary<SettingType, UIButton>();
            CreateMenuIcon(parent, iconClick);        
            CreateWindow(parent);
        }
    }
    protected static void CreateWindow(GameObject parent)
    {
        DDGUIAdvancedWidgetsCreator creator = new DDGUIAdvancedWidgetsCreator();
        creator.ChildrenWidth += 75;
        serverWindow = creator.CreateBasicWindow(parent, Localization.Get("DDS ServerPanel WindowTitle"),"");
        statsLabel = creator.AddLabel(Localization.Get("DDS ServerPanel WindowLabel1"));
        int height = 50;
        creator.AddLabel(creator.ChildrenWidth, height, Localization.Get("DDS ServerPanel WindowLabel2"));
        foreach (SettingType settingType in SettingType.GetValues(typeof(SettingType)))
        {

            settingButtonDictionary.Add(settingType, creator.AddBigButton(creator.ChildrenWidth, height, new EventDelegate(), ""));
        }
        UIWindow.Show(serverWindow);
        UIWindow.Hide(serverWindow);
    }
    public static void ShowWindow()
    {
        UIWindow.Show(serverWindow);
    }
    public static void SetTextOfSetting(SettingType type, string text)
    {
        if (settingButtonDictionary.ContainsKey(type))
            settingButtonDictionary[type].GetComponentInChildren<UILabel>().text = text;
    }
    public static void AddChangeSettingEventDelegate(SettingType type, EventDelegate eventDelegate)
    {
        if (settingButtonDictionary.ContainsKey(type))
        {
            //NGUIDebug.Log(type.ToString());
            settingButtonDictionary[type].onClick.Add(eventDelegate);
        }
    }
    protected static void CreateMenuIcon(GameObject parent, EventDelegate iconClick)
    {
        UIFont font = DDGUIResources.GetMediumUIFont();
        UIAtlas atlas = DDGUIResources.GetWindwardUIAtlas();
        GameObject playersHudButtonGameObject = GameObject.Find("HUD Button - Who All");
        //check: http://stackoverflow.com/a/25683073
        //Button is the name of the sprite in the atlas
        UIWidgetContainer widgetContainer = NGUITools.AddChild<UIWidgetContainer>(parent);
        GameObject prefab = DDGUIResources.GetHUDButtonPrefab();
        GameObject prefabChild = NGUITools.AddChild(parent, prefab);
        prefabChild.GetComponentInChildren<UILocalize>().key = "DDS ServerPanel IconText";

        prefabChild.GetComponentInChildren<UISprite>().spriteName = "Item - Scroll";
        UIWidget background = prefabChild.GetComponent<UIWidget>();
        background.SetAnchor(playersHudButtonGameObject, 0, -150, 35, -140);
        prefabChild.GetComponent<UIButton>().onClick.Add(iconClick);
        Icon = background;

    }
}
