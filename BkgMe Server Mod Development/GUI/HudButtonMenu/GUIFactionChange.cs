﻿using UnityEngine;
using Tasharen;
using DDGUI;

class GUIFactionChange
{
    public static UIWidget Icon;
    static UIPanel factionChangeWindow;
    static UIPanel factionCreationWindow;
    public static void Initialize(GameObject parent)
    {
        if (parent != null)
            CreateMenuIcon(parent);
    }
    protected static void CreateMenuIcon(GameObject parent)
    {    
        UIFont font = DDGUIResources.GetMediumUIFont();
        UIAtlas atlas = DDGUIResources.GetWindwardUIAtlas();
        GameObject playersHudButtonGameObject = GameObject.Find("HUD Button - Who All");
        //check: http://stackoverflow.com/a/25683073
        //Button is the name of the sprite in the atlas
        UIWidgetContainer widgetContainer = NGUITools.AddChild<UIWidgetContainer>(parent);
        GameObject prefab = DDGUIResources.GetHUDButtonPrefab();
        GameObject prefabChild = NGUITools.AddChild(parent, prefab);
        prefabChild.GetComponentInChildren<UILocalize>().key = "DDS FactionChange IconText";

        prefabChild.GetComponentInChildren<UISprite>().spriteName = "Item - Black Flag";
        UIWidget background = prefabChild.GetComponent<UIWidget>();
        background.SetAnchor(playersHudButtonGameObject,0,-115,35,-105);
        prefabChild.GetComponent<UIButton>().onClick.Add(new EventDelegate(HandleFactionIconClick));
        Icon = background;
    }
    public static void HandleFactionIconClick()
    {
        if (!MyPlayerCustomValues.SelectedSecondFaction)
        {
            if (factionCreationWindow==null)
                CreateFactionCreationWindow();
            UIWindow.Show(factionCreationWindow);
        }
        else
        {
            if (factionChangeWindow == null)
                CreateFactionChangeWindow();
            UIWindow.Show(factionChangeWindow);
        }
           
    }

    protected static void CreateFactionChangeWindow()
    {
        int firstFactionID = MyPlayer.factionID;
        int secondFactionID = MyPlayerManager.GetSecondFactionIDOfLocalPlayer();
        string firstFactionName = FactionManager.GetName(firstFactionID);
        string secondFactionName = FactionManager.GetName(secondFactionID);
        GameObject hud = DDGUIHelper.GetGameGUIParentForNewElements();
        DDGUIAdvancedWidgetsCreator windowCreator = new DDGUIAdvancedWidgetsCreator();
        factionChangeWindow = windowCreator.CreateBasicWindow(hud, 
            Localization.Get("DDS FactionChange Window1Title"),
            string.Format(Localization.Get("DDS FactionChange Window1Desc"),
            DDGUIHelper.SetColorForMessageInUILabel(firstFactionName, FactionManager.GetColor((FactionManager.FactionID)firstFactionID)),
            DDGUIHelper.SetColorForMessageInUILabel(secondFactionName, FactionManager.GetColor((FactionManager.FactionID)secondFactionID))));
        windowCreator.AddBigButton(new EventDelegate(HandleTeleportButtonClick),
                string.Format(Localization.Get("DDS FactionChange Window1ButtonText " + secondFactionID)));


    }
    protected static void CreateFactionCreationWindow()
    {
        int firstFactionID = MyPlayer.factionID;
        GameObject hud = DDGUIHelper.GetGameGUIParentForNewElements();
        DDGUIAdvancedWidgetsCreator windowCreator = new DDGUIAdvancedWidgetsCreator();
        windowCreator.TextHeight = 180;
        factionCreationWindow = windowCreator.CreateBasicWindow(hud,
            Localization.Get("DDS FactionChange Window2Title"),
            string.Format(Localization.Get("DDS FactionChange Window2Desc "+firstFactionID),
            DDGUIHelper.SetColorForMessageInUILabel(FactionManager.GetName(MyPlayer.factionID), FactionManager.GetColor((FactionManager.FactionID)firstFactionID))
            , DDGUIHelper.SetColorForMessageInUILabel(FactionManager.GetName(MyPlayer.factionID), FactionManager.GetColor((FactionManager.FactionID)firstFactionID))));

        switch (firstFactionID)
        { 
            case 1:

                windowCreator.AddBigButton(new EventDelegate(()=>HandleFactionChange(2)), string.Format(Localization.Get("DDS FactionChange Window2ButtonText " + firstFactionID)));
                break;
            case 2:
                windowCreator.AddBigButton(new EventDelegate(() => HandleFactionChange(1)), string.Format(Localization.Get("DDS FactionChange Window2ButtonText 2 1")));
                windowCreator.AddBigButton(new EventDelegate(() => HandleFactionChange(3)), string.Format(Localization.Get("DDS FactionChange Window2ButtonText 2 3")));
                windowCreator.AddBigButton(new EventDelegate(() => HandleFactionChange(4)), string.Format(Localization.Get("DDS FactionChange Window2ButtonText 2 4")));
                break;
            case 3:
                windowCreator.AddBigButton(new EventDelegate(() => HandleFactionChange(2)), string.Format(Localization.Get("DDS FactionChange Window2ButtonText " + firstFactionID)));
                break;
            case 4:
                windowCreator.AddBigButton(new EventDelegate(() => HandleFactionChange(2)), string.Format(Localization.Get("DDS FactionChange Window2ButtonText " + firstFactionID)));
                break;
        }

    }

    public static void HandleTeleportButtonClick()
    {
        MyPlayerManager.HandleFactionSwitch();
        UIWindow.Hide(factionChangeWindow);       
    }
    public static void HandleFactionChange(int secondFactionID)
    {
        MyPlayerManager.SetSecondFaction(secondFactionID);
        UIWindow.Hide(factionCreationWindow);
    }

}

