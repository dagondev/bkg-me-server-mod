﻿using DDGUI;
using UnityEngine;
using System;

class GUIMainMenuRightPanel
{
    static UIPanel updateWindow;
    static UIPanel feedbackWindow;
    static UIPanel rightPanel;
    static UILabel[] labels;
    static UIButton updateToNewestButton;
    static UIButton movePanelButton;
    static UILabel feedbackStatusLabel;
    static DateTime lastfeedbackSend;
    static string feedbackMsg;
    static int progress;

    public static string NewsLabelText { set { if(labels[0]!=null) labels[0].text = value; } }
    public static string PlayersOnlineLabelText { set { if (labels[1] != null) labels[1].text = value; } }
    public static string MaintenanceLabelText { set { if (labels[2] != null) labels[2].text = value; } }
    public static string ClientVersionLabelText { set { if (labels[3] != null) labels[3].text = value; } }
    public static string NewestLabelText { set { if (labels[4] != null) labels[4].text = value; } }
    public static int DownloadingProgressValue { set { progress = value; } }
    public static void UpdateToNewestButtonUpdate(bool enabled, bool downloadingUpdate)
    {
        if (!MainMenuManager.update)
            return;
        if (updateToNewestButton == null)
            return;
        UILabel label = updateToNewestButton.GetComponentInChildren<UILabel>();
        if(label==null)
            return;
        label.color = Color.white;
        if (enabled)
        {
            if (downloadingUpdate)
            {
                label.text = string.Format(Localization.Get("DDS MainMenu UpdateToNewest3"), progress);
            }
            else
            {
                updateToNewestButton.enabled = true;
                label.text = Localization.Get("DDS MainMenu UpdateToNewest");
            }
        }
        else
        {
            updateToNewestButton.enabled = false;
            label.text = Localization.Get("DDS MainMenu UpdateToNewest2");
        }
    }
    public static UIPanel Init(GameObject parent, EventDelegate panelClosingEvent, EventDelegate handleUpdatingClient, bool isWindows)
    {

        DateTime buildTime = Utils.RetrieveLinkerTimestamp();
        lastfeedbackSend = DateTime.Now.AddSeconds(-30);
        feedbackMsg = "";
        UIStatusBar.Show("BkgMe Server Mod Loaded. Builded: " + buildTime.ToString() + " CET (UTC +1:00); Game version: " + GameConfig.version);
        DDGUIAdvancedWidgetsCreator creatorPanel = new DDGUIAdvancedWidgetsCreator();
        rightPanel = creatorPanel.CreateBasicPanel(parent, Localization.Get("DDS ModName"),"", UIAnchor.Side.Right,false);
        //by nigdy nie zaslanialo main menu jesli okno gry jest male
        rightPanel.depth = 1;
        int height = 35;
        string loading = "Loading data...";
        labels = new UILabel[5];
        labels[0] = creatorPanel.AddLabel(creatorPanel.ChildrenWidth, height * 2, string.Format(Localization.Get("DDS MainMenu News"), loading));
        labels[1] = creatorPanel.AddLabel(creatorPanel.ChildrenWidth, height, string.Format(Localization.Get("DDS MainMenu Players"), loading));
        labels[2] = creatorPanel.AddLabel(creatorPanel.ChildrenWidth, height, string.Format(Localization.Get("DDS MainMenu Maintenance"), loading));
        labels[3] = creatorPanel.AddLabel(creatorPanel.ChildrenWidth, height, string.Format(Localization.Get("DDS MainMenu ClientVersion"), loading));
        labels[4] = creatorPanel.AddLabel(creatorPanel.ChildrenWidth, height, string.Format(Localization.Get("DDS MainMenu NewestVersion"), loading));

        updateToNewestButton = creatorPanel.AddBigButton(new EventDelegate(()=>{
            if (updateWindow == null)
                InitUpdateWindow(parent,isWindows,handleUpdatingClient);
            UIWindow.Show(updateWindow);
        }), Localization.Get("DDS MainMenu UpdateToNewest2"));
        updateToNewestButton.enabled = false;

        UIButton steamServerButton = creatorPanel.AddBigButton(new EventDelegate(() => WebsiteManager.OpenSteamServerThreadPage()), Localization.Get("DDS MainMenu SteamThreadServer"));
        steamServerButton.GetComponentInChildren<UILabel>().color = Color.white;

        UIButton steamModButton = creatorPanel.AddBigButton(new EventDelegate(() => WebsiteManager.OpenSteamModThreadPage()), Localization.Get("DDS MainMenu SteamThreadMod"));
        steamModButton.GetComponentInChildren<UILabel>().color = Color.white;

        UIButton feedbackButton = creatorPanel.AddBigButton(new EventDelegate(() =>
        {
            if (feedbackWindow == null)
                InitFeedbackWindow(parent,height);
            UIWindow.Show(feedbackWindow);
        }), Localization.Get("DDS MainMenu Feedback"));

        movePanelButton = creatorPanel.AddBigButton(new EventDelegate(() =>
        {
            if (rightPanel.depth == 1)
            {
                movePanelButton.GetComponentInChildren<UILabel>().text = Localization.Get("DDS MainMenu MovePanel2");
                rightPanel.depth = 2;
            }
            else
            {
                movePanelButton.GetComponentInChildren<UILabel>().text = Localization.Get("DDS MainMenu MovePanel1");
                rightPanel.depth = 1;
            }

        }), Localization.Get("DDS MainMenu MovePanel1"));
        return rightPanel;
    }
    static void InitUpdateWindow(GameObject parent, bool isWindows, EventDelegate handleUpdatingClient)
    {
        DDGUIAdvancedWidgetsCreator creatorWindow = new DDGUIAdvancedWidgetsCreator();
        string desc;
        if (isWindows)
            desc = Localization.Get("DDS UpdateWindow Desc Win");
        else
            desc = Localization.Get("DDS UpdateWindow Desc Linux");
        updateWindow = creatorWindow.CreateBasicWindow(parent, Localization.Get("DDS UpdateWindow Title"));
        creatorWindow.AddLabel(creatorWindow.ChildrenWidth, 300, desc);
        creatorWindow.AddBigButton(handleUpdatingClient, Localization.Get("DDS UpdateWindow Desc Button"));
    }
    static void InitFeedbackWindow(GameObject parent, int height)
    {
        DDGUIAdvancedWidgetsCreator creatorWindow = new DDGUIAdvancedWidgetsCreator();
        feedbackWindow = creatorWindow.CreateBasicWindow(parent, Localization.Get("DDS MainMenu Feedback"), Localization.Get("DDS MainMenu Feedback Desc"));
        UIInput feedbackInput = creatorWindow.AddInput(creatorWindow.ChildrenWidth, height * 6, true, 20, 10000, Localization.Get("DDS MainMenu Feedback StartText"));
        feedbackInput.onChange.Add(new EventDelegate(() => feedbackMsg = feedbackInput.value));
        feedbackStatusLabel = creatorWindow.AddLabel(creatorWindow.ChildrenWidth, height * 2, "");
        creatorWindow.AddBigButton(new EventDelegate(() =>
        {
            HandleSendingFeedback();
        }), Localization.Get("DDS MainMenu Feedback ButtonSend"));

    }
    static void HandleSendingFeedback()
    {
        if (lastfeedbackSend > DateTime.Now.AddSeconds(-30))
        {
            feedbackStatusLabel.text = Localization.Get("DDS MainMenu FeedBack Spam");
            lastfeedbackSend = DateTime.Now;
            return;
        }

        if (string.IsNullOrEmpty(feedbackMsg))
        {
            feedbackStatusLabel.text = Localization.Get("DDS MainMenu Feedback Empty");
            return;
        }
        feedbackStatusLabel.text = Localization.Get("DDS MainMenu Feedback Progress");
        if (WebsiteManager.SendFeedback(feedbackMsg))
        {
            UIWindow.Hide(feedbackWindow);
            UIStatusBar.Show(Localization.Get("DDS MainMenu Feedback Sended"));
        }
        else
            feedbackStatusLabel.text = "There was an error on server side... Try again later or copy your message and write at steam forum or send me a mail at mailaddress";
        lastfeedbackSend = DateTime.Now;
    }
}
