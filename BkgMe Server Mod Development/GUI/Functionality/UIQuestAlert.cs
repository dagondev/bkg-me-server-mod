﻿using DDGUI;
using UnityEngine;

class UIQuestAlert
{
    static UIPanel questWindow;
    static UILabel questTextLabel;
    static UILabel questTitleLabel;

    protected static void CreateWindow()
    {
        DDGUIAdvancedWidgetsCreator creator = new DDGUIAdvancedWidgetsCreator();
        creator.TextHeight = 120;
        questWindow = creator.CreateBasicWindow(DDGUIHelper.GetGameGUIParentForNewElements(), Localization.Get("DDS Quests Alert Title"));
        questTextLabel = creator.AddLabel(creator.ChildrenWidth, 250, creator.TextFont, creator.TextFont.defaultSize, "");
        questTitleLabel = creator.TitleLabel;
        questTextLabel.color = Color.black;
        
    }
    public static void ShowQuestAlert(string chatAndWindowText, string statusBarText, string titleWindowText)
    {
        ShowQuestAlert(chatAndWindowText, statusBarText);
        questTitleLabel.text = titleWindowText;
    }
    public static void ShowQuestAlert(string chatAndWindowText, string statusBarText)
    {
        if (questWindow == null)
            CreateWindow();
        questTextLabel.text = chatAndWindowText;
        UIWindow.Show(questWindow);
        //UIGameChat.Add(chatAndWindowText, GameChatMessagePrinter.SERVER_INFO_SPECIAL_COLOR);
        if (!statusBarText.Equals(""))
            UIStatusBar.Show(statusBarText);
    }
    public static void ShowLocalizedQuestAlert(string chatAndWindowTextID, string statusBarTextID)
    {
        ShowLocalizedQuestAlert(chatAndWindowTextID, statusBarTextID, "DDS Quests Alert Title");
    }
    public static void ShowLocalizedQuestAlert(string chatAndWindowTextID, string statusBarTextID, string titleWindowTextID)
    {
        ShowQuestAlert(Localization.Get(chatAndWindowTextID), Localization.Get(statusBarTextID), Localization.Get(titleWindowTextID));
    }
}

