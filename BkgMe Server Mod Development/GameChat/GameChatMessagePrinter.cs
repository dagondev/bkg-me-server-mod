﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;
using DDGUI;

/**
 * Klasa definiuje jak wygladaja wiadomosci wyswietlane w czacie
 */
class GameChatMessagePrinter
{
    public static Color SERVER_TEXT_COLOR = new Color(193f/255f, 163f/255f, 115f/255f);
    public static Color SERVER_SUCCESS_COLOR = Color.green;
    public static Color SERVER_INFO_SPECIAL_COLOR = Color.grey;
    public static Color SERVER_INFO_COLOR = Color.white;
    public static Color WEBSITE_LINK_COLOR = new Color(112f/255f, 217f/255f, 255f/255f);
    public static Color SERVER_WARNING_COLOR = Color.yellow;
    public static Color SERVER_ERROR_COLOR = new Color(255f / 255f, 165f / 255f, 0f / 255f);
    public static Color SERVER_COMMAND_COLOR = SERVER_ERROR_COLOR;
    public static Color PANEL_WARNING_TEXT_COLOR = new Color(1,1, 0);
    public static Color PANEL_TEXT_COLOR = new Color(44 / 255f, 103 / 255f, 0 / 255f);
    public static Color BKGME_COLOR = PANEL_TEXT_COLOR;

    public static void PrintErrorMessage(string error)
    {
        UIGameChat.Add("BkgMe Server Mod Error:", SERVER_ERROR_COLOR);
        UIGameChat.Add(error, SERVER_WARNING_COLOR);
    }
    public static void PrintUnknownServerCommand(string command)
    {
        UIGameChat.Add(Localization.Get("DDS UnknownCommandMsg") + ":", SERVER_TEXT_COLOR);
        UIGameChat.Add(command, SERVER_COMMAND_COLOR);
        UIGameChat.Add(Localization.Get("DDS CommandsListMsg"),SERVER_ERROR_COLOR);
    }
    public static void PrintWelcomeMessage(string playerName)
    {
        UIGameChat.Add(DDGUIHelper.SetColorForMessageInUILabel(playerName, SERVER_COMMAND_COLOR) + ", " + Localization.Get("DDS WelcomeMsg"), SERVER_TEXT_COLOR);
        UIGameChat.Add(Localization.Get("DDS WelcomeDesc"), SERVER_TEXT_COLOR);
        UIGameChat.Add(Localization.Get("DDS WebsiteMsg"), SERVER_TEXT_COLOR);
        UIGameChat.Add("[u]http://www.bkg.me/[/u]", WEBSITE_LINK_COLOR);
    }

    public static void PrintBaseServerCommand(int playersOnline, bool maintenanceNeeded, string maintenanceInfo, string maintenanceDate)
    {
        UIGameChat.Add(Localization.Get("DDS Name") + " 3.1", SERVER_TEXT_COLOR);
        string maintenance = Localization.Get("DDS BasicInfoMsg Maintenance") + ": ";
        if (maintenanceNeeded)
        {
            maintenance += "[b][u]" + DDGUIHelper.SetColorForMessageInUILabel(maintenanceDate, SERVER_WARNING_COLOR) + " - " + DDGUIHelper.SetColorForMessageInUILabel(maintenanceInfo, SERVER_ERROR_COLOR) + "[/b][/u]";
            UIGameChat.Add(maintenance, SERVER_ERROR_COLOR);
        }
        else
        {
            maintenance += DDGUIHelper.SetColorForMessageInUILabel(maintenanceInfo, SERVER_SUCCESS_COLOR);
            UIGameChat.Add(maintenance, SERVER_TEXT_COLOR);
        }


        if (playersOnline > 0)
        {
            string playersOnlineString = "" + playersOnline;
            if (playersOnline > 1)
                playersOnlineString = DDGUIHelper.SetColorForMessageInUILabel(playersOnlineString, SERVER_SUCCESS_COLOR);
            else
                playersOnlineString = DDGUIHelper.SetColorForMessageInUILabel(playersOnlineString, SERVER_ERROR_COLOR);
            UIGameChat.Add(Localization.Get("DDS BasicInfoMsg PlayerOnline") + ": " + playersOnlineString, SERVER_TEXT_COLOR);
        }
        UIGameChat.Add(Localization.Get("DDS BasicInfoMsg LatestVersion") + ": " + WebsiteManager.GetNewestModVersionAsString(), SERVER_TEXT_COLOR);
        string announcement = WebsiteManager.GetAnnouncementTime().ToShortDateString()+";"+WebsiteManager.GetAnnouncementTime().ToShortTimeString()+"\n"+WebsiteManager.GetAnnoucementText();
        UIGameChat.Add(Localization.Get("DDS BasicInfoMsg LatestNews") + ": " +announcement, SERVER_TEXT_COLOR);
    }
    public static void PrintHelpServerCommand()
    {
        UIGameChat.Add(Localization.Get("DagonDenServer CommandsListTitle"), SERVER_TEXT_COLOR);
        UIGameChat.Add(Localization.Get("DagonDenServer CommandsListMsg"), SERVER_COMMAND_COLOR);
    }
    public static void PrintAboutServerCommand()
    {
        UIGameChat.Add(Localization.Get("DDS AboutTitle"), SERVER_TEXT_COLOR);
        UIGameChat.Add(Localization.Get("DDS AboutMsg"), SERVER_INFO_SPECIAL_COLOR);
    }
    public static void PrintSeasonServerCommand()
    {
        UIGameChat.Add(Localization.Get("DDS ScenarioMsg") + ":", SERVER_TEXT_COLOR);
        UIGameChat.Add(Localization.Get("DDS S3 ScenarioDesc"), SERVER_INFO_SPECIAL_COLOR);
        UIGameChat.Add(Localization.Get("DDS ObjectivesMsg") + ":", SERVER_TEXT_COLOR);
        for (int i = 0; i < 10; i++)
        {
            if (Localization.Exists("DDS S3 Objective" + i + "Desc"))
                UIGameChat.Add(i + ". " + Localization.Get("DDS S3 Objective" + i + "Desc"), SERVER_TEXT_COLOR);
        }
    }
    public static void PrintProgressServerCommand(int numberOflvlTownsForConsulate)
    {
        UIGameChat.Add(Localization.Get("DDS ObjectivesMsg") + ":", SERVER_TEXT_COLOR);
        UIGameChat.Add(Localization.Get("DDS S3 Objective1ShortMsg"), SERVER_TEXT_COLOR);
        UIGameChat.Add(Localization.Get("DDS S3 Objective3ShortMsg"), SERVER_TEXT_COLOR);
        UIGameChat.Add(Localization.Get("DDS CheckMapProgressMsg"), SERVER_INFO_SPECIAL_COLOR);
        UIGameChat.Add(Localization.Get("DDS S3 Objective2ShortMsg"), SERVER_TEXT_COLOR);
        UIGameChat.Add("Towns level 10: " + numberOflvlTownsForConsulate, SERVER_SUCCESS_COLOR);
    }
    public static void SendMaintenanceBroadcastMessage(string maintenanceInfo, string maintenanceDate)
    {
        string msg = DDGUIHelper.SetColorForMessageInUILabel(Localization.Get("DDS ServerMsg") + ": ", SERVER_TEXT_COLOR);
        msg += DDGUIHelper.SetColorForMessageInUILabel(Localization.Get("DDS BasicInfoMsg Maintenance") + ": ", SERVER_TEXT_COLOR);
        msg += "[b][u]" + DDGUIHelper.SetColorForMessageInUILabel(maintenanceDate, SERVER_WARNING_COLOR) + " - " + DDGUIHelper.SetColorForMessageInUILabel(maintenanceInfo, SERVER_ERROR_COLOR) + "[/b][/u]";
        UIGameChat.Add(msg, SERVER_TEXT_COLOR);
        UIStatusBar.Show(msg);
    }
    public static void PrintHrTag()
    {
        UIGameChat.Add("=====", Color.green);
    }
    public static void PrintHrLongTag()
    {
        UIGameChat.Add("============", Color.green);
    }
    

}

