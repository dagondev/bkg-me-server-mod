﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;
using System.Text.RegularExpressions;
using System;


//nasluchuje komend w postaci '/server.*', ktore moze wywolac gracz przez czat, i zajmuje sie ich przetworzeniem
class GameChatExtender
{
    static Regex regexServer = new Regex(@"^server(.*)");
    static Regex regexServer2 = new Regex(@"^srv(.*)");

    public static void HandleChatCommand(string text, ref bool isHandled) 
    {
        Match serverMatch = regexServer2.Match(text);
        //sprawdz wpierw srv, jak sie nie uda to server
        if (!serverMatch.Success)
        {
            serverMatch = regexServer.Match(text);
            if (!serverMatch.Success)
                return;
        }
        isHandled = true;
        string msg = serverMatch.Groups[1].Value;
        //UIGameChat.Add("Your msg:'" + msg + "'");
        HandleServerCommand(msg); 
    }
    protected static void HandleServerCommand(string text) 
    {
        switch (text)
        {
            case "":
            case " ":
                HandleBaseServerCommand();
                break;
            case " help":
                HandleHelpServerCommand();
                break;
            case " about":
                GameChatMessagePrinter.PrintAboutServerCommand();
                break;
            case " season":
                GameChatMessagePrinter.PrintSeasonServerCommand();
                break;
            case " progress":
                HandleProgressCommand();
                break;
            case " admin":
                ServerAdminTools.HandleAdminToolsShowRequest();
                break;
            default:
                GameChatMessagePrinter.PrintUnknownServerCommand(text);
                break;
        }
    }
    public static void HandleHelpServerCommand()
    {
        GameChatMessagePrinter.PrintHelpServerCommand();
    }
    public static void HandleBaseServerCommand() 
    {
        bool scheduled = WebsiteManager.GetIfServerMaintenanceIsNeeded();
        string maintenanceInfo = Localization.Get("DDS MaintenanceMsg NotPlanned");
        string maintenanceDate = "";
        int playersOnline = 0;
        //TODO: wpakowac to w metody getString,getFloat etc?
        if (scheduled)
        {
            maintenanceInfo = WebsiteManager.GetServerMaintenanceMessage();
            maintenanceDate = WebsiteManager.GetServerMaintenanceDateString();
        }
       playersOnline = WebsiteManager.GetPlayersOnlineCount();
        GameChatMessagePrinter.PrintBaseServerCommand(playersOnline, scheduled, maintenanceInfo, maintenanceDate);

    }

    protected static void HandleProgressCommand()
    {
        GameChatMessagePrinter.PrintProgressServerCommand(WebsiteManager.GetNumberOf10LevelTowns());
    }

}

