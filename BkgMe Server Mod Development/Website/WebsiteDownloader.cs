﻿using System.Net;
using System;
using System.ComponentModel;

//zajmuje sie sciaganiem danych ze strony
internal class WebsiteDownloader
{
    static string baseUrl = "http://www.bkg.me/serverstats/json/server-mod/";
    static string baseFileUrl = "http://www.bkg.me/static/server-mod/";
    static string urlPathMaintenance = "maintenance/";
    static string urlPathSeasonProgress = "season/progress/";
    static string urlPathBasicServerInfo = "basic/";
    static string urlPathFeedback = "send/feedback/";
    static string urlRelativePathFileNewestVersionTgz = "BkgMe%20Server%20Mod.tgz";
    static string urlRelativePathFileNewestVersionExe = "BkgMe%20Server%20Mod.exe";
    static string urlRelativePathFileNewestVersionSh = "install.sh";

    internal static string emptyJSON;

    internal static string basicServerInfoJSON;
    internal static string seasonProgressJSON;

    internal static DateTime basicServerLastUpdate;
    internal static DateTime seasonProgressLastUpdate;

    internal static void Init() 
    {
        emptyJSON="{}";
        basicServerInfoJSON = emptyJSON;
        seasonProgressJSON = emptyJSON;
    }

    internal static void ScheduleDownloadingBasicServerInfo()
    {
        DownloadASyncJSONString(DownloadBasicInfoJSONCompletedEventHandler, urlPathBasicServerInfo);
    }
    internal static void ScheduleDownloadingSeasonProgressInfo()
    {
        DownloadASyncJSONString(DownloadSeasonProgressJSONCompletedEventHandler, urlPathSeasonProgress);
    }
    internal static void ScheduleDownloadingNewestVersionOfTheMod(bool installer, AsyncCompletedEventHandler handler, DownloadProgressChangedEventHandler changedHandler, string fileName)
    {
        if (installer)
            DownloadASyncNewestVersion(handler, changedHandler, urlRelativePathFileNewestVersionExe, fileName);
        else
        {
            DownloadASyncNewestVersionScript(urlRelativePathFileNewestVersionSh, urlRelativePathFileNewestVersionSh);
            DownloadASyncNewestVersion(handler, changedHandler, urlRelativePathFileNewestVersionTgz, fileName);
           
        }
    }
    internal static string SendFeedback(string msg)
    {
        try
        {
            string result = "";
            System.Collections.ArrayList arrayList = new System.Collections.ArrayList(2);
            arrayList.Add("msg");
            arrayList.Add(msg);
            string parsedContent = JSONParser.jsonEncode(arrayList);
            //NGUIDebug.Log(parsedContent);
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                result = client.UploadString(baseUrl+urlPathFeedback, "POST", parsedContent);
            }
            return result;       
        }
        catch (Exception e)
        {
            NGUIDebug.Log(e.ToString());
            return e.ToString();
        }
    }
    //====
    protected static void DownloadBasicInfoJSONCompletedEventHandler(object sender, DownloadStringCompletedEventArgs e)
    {
        basicServerInfoJSON = handleCompletedEvent(e, ref basicServerLastUpdate);
    }
    protected static void DownloadSeasonProgressJSONCompletedEventHandler(object sender, DownloadStringCompletedEventArgs e)
    {
        seasonProgressJSON = handleCompletedEvent(e, ref seasonProgressLastUpdate);
    }
    protected static void DownloadASyncJSONString(DownloadStringCompletedEventHandler handler, string relativePath)
    {
        using (var webClient = new WebClient())
        {
            webClient.DownloadStringCompleted += handler;
            webClient.DownloadStringAsync(new Uri(baseUrl + relativePath));
        }
    }
    protected static void DownloadASyncNewestVersion(AsyncCompletedEventHandler downloaderHandler, DownloadProgressChangedEventHandler changedHandler, string relativePath,string fileName)
    {
        using (var webClient = new WebClient())
        {
            webClient.DownloadFileCompleted += downloaderHandler;
            //sciaga do folderu z gra
            webClient.DownloadProgressChanged += changedHandler;
            webClient.DownloadFileAsync(new Uri(baseFileUrl + relativePath),fileName);
        }       
    }
    protected static void DownloadASyncNewestVersionScript(string relativePath, string fileName)
    {
        using (var webClient = new WebClient())
        {
            webClient.DownloadFileAsync(new Uri(baseFileUrl + relativePath), fileName);
        }
    }
    //jesli sie udalo to value, jesli nie to "{}"
    protected static string handleCompletedEvent(DownloadStringCompletedEventArgs e, ref DateTime lastDateTime)
    {
        lastDateTime = DateTime.Now;
        if (e.Cancelled)
        {
            GameChatMessagePrinter.PrintErrorMessage(e.Error.ToString());
            return emptyJSON;
        }
        return e.Result;
    }
}

