﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;
using System;
using System.Net;
using System.ComponentModel;


//zajmuje sie przekladaniem infomacji otrzymanych ze strony na informacje potrzebne modowi i odwrotnie
class WebsiteManager
{
    //nie sciagaj nowych danych jesli ostatnie dane byly sciagane mniej niz:
    static double secondsOffset = 1;

    private static DateTime basicServerInfoJSONLastUpdate;
    private static DateTime seasonProgressInfoJSONLastUpdate;
    
    private static Hashtable basicServerInfoJSON;
    private static Hashtable seasonProgressInfoJSON;
    private const string websiteURL = "http://www.bkg.me";
    private const string steamServerThreadURL = "http://steamcommunity.com/app/326410/discussions/0/627457521172664817/";
    private const string steamModThreadURL = "http://steamcommunity.com/app/326410/discussions/2/620703493331750134/";
    private const string downloadPageURL = "http://www.bkg.me/static/server-mod/";
    public static void Init()
    {
        basicServerInfoJSON = new Hashtable();
        seasonProgressInfoJSON = new Hashtable();
        basicServerInfoJSONLastUpdate = DateTime.Now;
        seasonProgressInfoJSONLastUpdate = DateTime.Now;
        WebsiteDownloader.Init();
        WebsiteDownloader.ScheduleDownloadingBasicServerInfo();
        WebsiteDownloader.ScheduleDownloadingSeasonProgressInfo();
    }
    public static void ScheduleDownloadOfNewestVersion(bool installer, AsyncCompletedEventHandler downloadHandler, DownloadProgressChangedEventHandler progressHandler, string fileName)
    {
        WebsiteDownloader.ScheduleDownloadingNewestVersionOfTheMod(installer,downloadHandler, progressHandler,fileName);
    }
    public static void ScheduleUpdateOfAllData()
    {
            basicServerInfoJSONLastUpdate = DateTime.Now;
            WebsiteDownloader.ScheduleDownloadingBasicServerInfo();
            seasonProgressInfoJSONLastUpdate = DateTime.Now;
            WebsiteDownloader.ScheduleDownloadingSeasonProgressInfo();
    }
    public static void ParseAllJsons()
    {
        UpdateJSONHashTable(ref basicServerInfoJSON, ref basicServerInfoJSONLastUpdate, WebsiteDownloader.basicServerInfoJSON);
        UpdateJSONHashTable(ref seasonProgressInfoJSON, ref seasonProgressInfoJSONLastUpdate, WebsiteDownloader.seasonProgressJSON);        
    }
    //TODO: zamienic je na gettery! i przeniesc stringi do enuma!
    public static DateTime GetAnnouncementTime()
    {
        return Utils.getDateTimeFromHashTable(basicServerInfoJSON, "announcement_date");
    }
    public static string GetAnnoucementText()
    {
        return Utils.GetStringFromHashTable(basicServerInfoJSON, "announcement_text");
    }
    public static string GetNewestModVersionAsString()
    {
        return Utils.GetStringFromHashTable(basicServerInfoJSON, "mod_version");
    }
    public static int GetNumberOf10LevelTowns()
    {
        return Utils.GetIntFromHashTable(seasonProgressInfoJSON, "10lvl_towns");
    }
    public static bool GetIfServerMaintenanceIsNeeded()
    {
        return Utils.GetBoolFromHashTable(basicServerInfoJSON, "scheduled");
    }
    public static string GetServerMaintenanceMessage()
    {
        return Utils.GetStringFromHashTable(basicServerInfoJSON, "maintenance");        
    }
    public static DateTime GetServerMaintenanceDate()
    {
        return Utils.getDateTimeFromHashTable(basicServerInfoJSON, "from_date");
    }
    public static string GetServerMaintenanceDateString()
    {
        return GetServerMaintenanceDate().ToString();
    }
    public static int GetPlayersOnlineCount()
    {
        return Utils.GetIntFromHashTable(basicServerInfoJSON, "online");
    }
    public static void OpenBkgMePage() { OpenWebsitePage(websiteURL); }
    public static void OpenSteamServerThreadPage() { OpenWebsitePage(steamServerThreadURL); }
    public static void OpenSteamModThreadPage() { OpenWebsitePage(steamModThreadURL); }
    public static void OpenDownloadPage() { OpenWebsitePage(downloadPageURL); }
    protected static void OpenWebsitePage(string url)
    {
        System.Diagnostics.Process.Start(url);
    }
    public static bool SendFeedback(string msg)
    {
        string responseJSON = WebsiteDownloader.SendFeedback(msg);
        Hashtable jsonTable = Utils.retrieveJSONHashTable(responseJSON);
        if (jsonTable == null)
            return false;
        string response = Utils.GetStringFromHashTable(jsonTable, "result");
        //NGUIDebug.Log(response);
        switch (response)
        { 
            case "done":
                return true;
            default:
                return false;
        }
    }

    //nie sciagaj nowych danych, tylko zwroc stare, jesli ostatnie dane byly sciagane w ciagu ostatnich secondsOffset
    protected static Hashtable UpdateJSONHashTable(ref Hashtable jsonTable, ref DateTime jsonTableLastUpdate, string jsonString)
    {
        jsonTable = Utils.retrieveJSONHashTable(jsonString);
        jsonTableLastUpdate = DateTime.Now;

        return jsonTable;
    }

}
