﻿using UnityEngine;
class LocalizationManager
{
    static int maxNewDayComment;
    static int maxNewWeekComment;
    static int maxNewMonthComment;
    static int maxNewYearComment;
    static int maxQuestTransportGoodsToLighthouseComment;
    static int maxQuestSupplyRepairsOfTowersComment;
    static int maxShipWreckComment;
    public static int MaxNewDayComment { get { return maxNewDayComment; } }
    public static int MaxNewWeekComment { get { return maxNewWeekComment; } }
    public static int MaxNewMonthComment { get { return maxNewMonthComment; } }
    public static int MaxNewYearComment { get { return maxNewYearComment; } }
    public static int MaxQuestTransportGoodsToLighthouseComment { get { return maxQuestTransportGoodsToLighthouseComment; } }
    public static int MaxQuestSupplyRepairsOfTowersComment { get { return maxQuestSupplyRepairsOfTowersComment; } }
    public static int MaxShipWreckComment { get { return maxShipWreckComment; } }
    public static void Init()
    {
        IterateOverLocalizationLines();
    }
    protected static void IterateOverLocalizationLines()
    {
        //TODO: zrobic Localization manager do tego typu rzeczy
        maxNewDayComment = 0;
        maxNewWeekComment = 0;
        maxNewMonthComment = 0;
        maxNewYearComment = 0;
        maxQuestTransportGoodsToLighthouseComment = 0;
        maxShipWreckComment = 0;
        bool atLeastOneExists = true;
        int i = 0;
        while (atLeastOneExists)
        {
            atLeastOneExists = false;
            if (Localization.Exists("DDS Events MidnightRemark " + i) && Localization.Exists("DDS Events NewDayRelief " + i))
            {
                atLeastOneExists = true;
                maxNewDayComment++;
            }
            if (Localization.Exists("DDS Events NewWeekComment " + i))
            {
                atLeastOneExists = true;
                maxNewWeekComment++;
            }
            if (Localization.Exists("DDS Events NewMonthComment " + i))
            {
                atLeastOneExists = true;
                maxNewMonthComment++;
            }
            if (Localization.Exists("DDS Events NewYearComment " + i))
            {
                atLeastOneExists = true;
                maxNewYearComment++;
            }
            if (Localization.Exists("QuestTransportGoodsToLighthouse Encounter2 " + i) && Localization.Exists("QuestTransportGoodsToLighthouse Encounter4 " + i))
            {
                atLeastOneExists = true;
                maxQuestTransportGoodsToLighthouseComment++;
            }
            if (Localization.Exists("QuestSupplyRepairsOfTowers Encounter2 " + i))
            {
                atLeastOneExists = true;
                maxQuestSupplyRepairsOfTowersComment++;
            }
            if (Localization.Exists("DDS Events ShipWreckSeen " + i))
            {
                atLeastOneExists = true;
                maxShipWreckComment++;
            }
            i++;
            if (i > 1000)
                break;
        }
    }
}
