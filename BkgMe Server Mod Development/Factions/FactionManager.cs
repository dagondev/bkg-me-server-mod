﻿using UnityEngine;

class FactionManager
{
    public enum FactionID
    {
        PIRATES=0,
        VALIANT=1,
        CONSULATE=2,
        SOJOURN=3,
        EXCHANGE=4,
        SYNDICATE=5,

    }
    public static GameConfig.Reaction OnGetReaction(int faction1, int faction2)
    {
        switch (faction1)
        { 
            case 0:
                return GameConfig.Reaction.Hostile;
            case 1:
                switch (faction2)
                { 
                    case 0:
                    case 3:
                    case 4:
                        return GameConfig.Reaction.Hostile;
                    default:
                        return GameConfig.Reaction.Neutral;
                }
            case 2:
                switch (faction2)
                {
                    case 0:
                        return GameConfig.Reaction.Hostile;
                    case 1:
                    case 3:
                    case 4:
                        return GameConfig.Reaction.Neutral;
                    default:
                        return GameConfig.Reaction.Friendly;
                }
            case 3:
                switch (faction2)
                {
                    case 0:
                    case 1:
                    case 4:
                        return GameConfig.Reaction.Hostile;
                    default:
                        return GameConfig.Reaction.Neutral;
                }
            case 4:
                switch (faction2)
                {
                    case 0:
                    case 1:
                    case 3:
                        return GameConfig.Reaction.Hostile;
                    default:
                        return GameConfig.Reaction.Neutral;
                }
            case 5:
                switch (faction2)
                {
                    case 0:
                        return GameConfig.Reaction.Hostile;
                    case 1:
                    case 3:
                    case 4:
                        return GameConfig.Reaction.Neutral;
                    default:
                        return GameConfig.Reaction.Friendly;
                }
        }
        return GameConfig.Reaction.Friendly;
    }
    public static Color GetColor(FactionID id)
    {
        switch (id)
        { 
            case FactionID.PIRATES:
                return Color.black;
            case FactionID.VALIANT:
                return Color.red;
            case FactionID.SOJOURN:
                return Color.blue;
            case FactionID.CONSULATE:
                return Color.green;
            case FactionID.EXCHANGE:
                return Color.yellow;
            case FactionID.SYNDICATE:
                return new Color(138, 43, 226);
            default:
                return Color.white;

        }

    }
    public static string GetName(int factionID)
    {
        if (factionID < 0 || factionID > 5)
            return "";
        return GetName((FactionID) factionID);
    }
    public static string GetName(FactionID id)
    {
        switch (id)
        {
            case FactionID.VALIANT:
                return "Valiant";
            case FactionID.CONSULATE:
                return "Consulate";
            case FactionID.SOJOURN:
                return "Sojourn";
            case FactionID.EXCHANGE:
                return "Exchange";
            case FactionID.PIRATES:
                return "Pirates";
            case FactionID.SYNDICATE:
                return "Syndicate";
            default:
                return "none";

        }
    }
}

