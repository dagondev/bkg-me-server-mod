﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;
using System;
using System.IO;

/**
 * Zbior metod przydatnych wszedzie ktore nie nalezaly do konkretnej kategorii
 */ 
class Utils
{
    public static string GetCurrentVersion()
    {
        if(IsThisRunningOnWindows())
        {
            using (StreamReader sr = new StreamReader(ServerModPath+"\\Version.txt"))
            {
                return sr.ReadToEnd();
            
            }
        } else 
        {
            using (StreamReader sr = new StreamReader(ServerModPath+"/Version.txt"))
            {
                return sr.ReadToEnd();
            
            }
        }
    }
    public static string ServerModPath
    {
        get
        {
            if (IsThisRunningOnWindows())
                return WindwardDocumentsPath + "\\Mods\\BkgMe Server Mod";
            else
                return WindwardDocumentsPath + "/Mods/BkgMe Server Mod";
        }
    }
    public static string WindwardDocumentsPath
    {
        get
        {
            if(IsThisRunningOnWindows())
                return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Windward";
            else
                return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/Windward";
        }
    }
    public static bool IsThisRunningOnWindows()
    {
        return Utils.GetOSType().Contains("Win");
    }
    public static string GetOSType()
    { 
        return Environment.OSVersion.Platform.ToString();
    }
    public static System.DateTime RetrieveLinkerTimestamp()
    {
        string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
        const int c_PeHeaderOffset = 60;
        const int c_LinkerTimestampOffset = 8;
        byte[] b = new byte[2048];
        System.IO.Stream s = null;

        try
        {
            s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            s.Read(b, 0, 2048);
        }
        finally
        {
            if (s != null)
            {
                s.Close();
            }
        }

        int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
        int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
        System.DateTime dt = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        dt = dt.AddSeconds(secondsSince1970);
        //dt = dt.ToLocalTime();
        return dt;
    }
    public static Hashtable retrieveJSONHashTable(string jsonString)
    {
        return (Hashtable)JSONParser.jsonDecode(jsonString);
    }
    //jesli jest to value, jesli nie to false
    public static bool GetBoolFromHashTable(Hashtable hashTable, string key)
    {
        bool value = false;
        if (hashTable.ContainsKey(key))
            value = ((bool)hashTable[key]);
        return value;
    }
    //jesli jest to value, jesli nie to ""
    public static string GetStringFromHashTable(Hashtable hashTable, string key)
    {
        string value = "";
        if (hashTable.ContainsKey(key))
            value = ((string)hashTable[key]);
        return value;
    }
    //jesli jest to value, jesli nie to -1
    public static int GetIntFromHashTable(Hashtable hashTable, string key)
    {
        int value = -1;
        if (hashTable.ContainsKey(key))
            value = (int)((double)hashTable[key]);
        return value;
    }

    //jesli jest to value, jesli nie to -1
    public static double GetDoubleFromHashTable(Hashtable hashTable, string key)
    {
        double value = -1;
        if (hashTable.ContainsKey(key))
            value = (double)hashTable[key];
        return value;
    }
    //jesli jest parsowalny value string to datetime, jesli nie to data z teraz
    public static DateTime getDateTimeFromHashTable(Hashtable hashTable, string key)
    {
        DateTime dateTime = DateTime.Now;
        string dateString = GetStringFromHashTable(hashTable,key);
        if(dateString=="")
            return dateTime;
        return DateTime.Parse(dateString);
    }
}

