﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;
using DDGUI;


class ServerAdminTools
{

    static bool wasAuthenticated=false;

    static string playerSelected;
    public static float time=1f;
    public static float intensivity=1f;
    public static void Update()
    {
        if (wasAuthenticated)
        {
            GUIServerAdminTools.UpdateLastTimeUpdateLabel("Last updated: " + System.DateTime.Now.ToLongTimeString());
        }
    }
    public static void Init()
    {
        if (Steamworks.steamID.Equals("76561198008376581"))
        {
            wasAuthenticated = true;
            playerSelected = TNManager.player.name;
        }
        else
            wasAuthenticated = false;

    }
    public static void HandleAdminToolsShowRequest()
    {
        if (wasAuthenticated)
        {
            GUIServerAdminTools.ShowWindow();
            return;
        }

    }


    public static void HandleFactionRegionConquering()
    {
        int factionID = MyPlayer.factionID;
        for (int i = 0; i < GameTown.list.Count; i++)
        {
            if (GameTown.list[i].GetType() == typeof(GameTown))
                GameTown.list[i].ChangeFaction(factionID);
        }
        for (int i = 0; i < Ship.list.Count; i++)
        {
            if (Ship.list[i].GetType() == typeof(Ship))
                Ship.list[i].ChangeFaction(factionID);
        }
        for (int i = 0; i < GuardTower.list.Count; i++)
        {
            if (GuardTower.list[i].GetType() == typeof(GuardTower))
                GuardTower.list[i].ChangeFaction(factionID);
        }
        for (int i = 0; i < Lighthouse.list.Count; i++)
        {
            if (Lighthouse.list[i].GetType() == typeof(Lighthouse))
                Lighthouse.list[i].ChangeFaction(factionID);
        }
        UIStatusBar.Show("Done"); 
    }
    public static void HandleDiscoveringEverything()
    {
        int factionID = MyPlayer.factionID;
        for (int i = 0; i < GameTown.list.Count; i++)
        {
            if (GameTown.list[i].GetType() == typeof(GameTown))
                GameTown.list[i].discoveredByPlayer = true;
        }
        for (int i = 0; i < Ship.list.Count; i++)
        {
            if (Ship.list[i].GetType() == typeof(Ship))
                Ship.list[i].discoveredByPlayer = true;
        }
        for (int i = 0; i < GuardTower.list.Count; i++)
        {
            if (GuardTower.list[i].GetType() == typeof(GuardTower))
                GuardTower.list[i].discoveredByPlayer = true;
        }
        for (int i = 0; i < Lighthouse.list.Count; i++)
        {
            if (Lighthouse.list[i].GetType() == typeof(Lighthouse))
                Lighthouse.list[i].discoveredByPlayer = true;
        }
        UIStatusBar.Show("Done");
    }

}
