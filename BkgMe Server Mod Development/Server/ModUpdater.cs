﻿
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using UnityEngine;
class ModUpdater
{
    public static bool CheckForNewVersion = true;
    public static bool DownloadingUpdate = false;
    static string version;
    static string exeFilename = "BkgMe Server Mod.exe";
    static string tgzFilename = "BkgMe Server Mod.tgz";
    public static void CheckIfNewVersionAppeared()
    {
        if (!CheckForNewVersion)
            return;
        //TODO: porobic we wszystkich gui tak jak jest tu i pousuwac wszelka logice stamtad
        string newestVersionString =WebsiteManager.GetNewestModVersionAsString();
        ulong newVersion=0;
        ulong.TryParse(newestVersionString, out newVersion);
        ulong currentVersion = 0;
        ulong.TryParse(Utils.GetCurrentVersion(), out currentVersion);
        if (newVersion > currentVersion)
        {
            CheckForNewVersion = false;
            if (Utils.IsThisRunningOnWindows())
            {
                WebsiteManager.ScheduleDownloadOfNewestVersion(true, DownloadExeFileCompletedEventHandler,MainMenuManager.DownloadingProgressChanged,exeFilename);
                UIStatusBar.Show(string.Format(Localization.Get("DDS UpdateProcess 1 Win"),newestVersionString));
            }
            else
            {
                WebsiteManager.ScheduleDownloadOfNewestVersion(false, DownloadZIPFileCompletedEventHandler, MainMenuManager.DownloadingProgressChanged, tgzFilename);
                UIStatusBar.Show(string.Format(Localization.Get("DDS UpdateProcess 1 Linux"), newestVersionString));
            }
            version=newestVersionString;
            DownloadingUpdate = true;
        }
    }
    public static void UpdateClient()
    {
        ProcessStartInfo processInfo;
        Process process;
        if (Utils.IsThisRunningOnWindows())
        {
            //NGUIDebug.Log("Uruchamiam instalke.");
            if (!File.Exists("BkgMe Server Mod.exe"))
            {
                //NGUIDebug.Log("Nie ma pliku.");
                UIStatusBar.Show("There is no BkgMe Server Mod.exe in the game folder. Consider downloading installer manually - just click " + Localization.Get("DDS MainMenu SteamThreadServer") + " button in the main menu panel.");
                return;
            }
            processInfo = new ProcessStartInfo("BkgMe Server Mod.exe");
            process = Process.Start(processInfo);
            Application.Quit();
        }
        else
        {
            if (!File.Exists("install.sh"))
            {
                //NGUIDebug.Log("Nie ma pliku.");
                UIStatusBar.Show("There is no install.sh in the game folder. Consider downloading script manually - just click " + Localization.Get("DDS MainMenu SteamThreadServer") + " button in the main menu panel.");
                return;
            }
            string argss = "chmod +x install";
            string verb = " "; 
            ProcessStartInfo procInfo = new ProcessStartInfo();
            procInfo.WindowStyle = ProcessWindowStyle.Normal;
            procInfo.UseShellExecute = false;
            procInfo.FileName = "sh"; // 'sh' for bash
            procInfo.Arguments = argss; // The Script name
            procInfo.Verb = verb; // ------------
            Process.Start(procInfo); // Start that process.

            argss = "install.sh";
            verb = " ";
            procInfo = new ProcessStartInfo();
            procInfo.WindowStyle = ProcessWindowStyle.Normal;
            procInfo.UseShellExecute = false;
            procInfo.FileName = "sh"; // 'sh' for bash
            procInfo.Arguments = argss; // The Script name
            procInfo.Verb = verb; // ------------
            Process.Start(procInfo); // Start that process.
            Application.Quit();
            //NGUIDebug.Log("Uruchamiam skrypt sh.");
        }
    }
    protected static void DownloadZIPFileCompletedEventHandler(object sender, AsyncCompletedEventArgs e)
    {
        UIStatusBar.Show(string.Format(Localization.Get("DDS UpdateProcess 2 Linux"), version));
        DownloadingUpdate = false;
    }
    protected static void DownloadExeFileCompletedEventHandler(object sender, AsyncCompletedEventArgs e)
    {
        UIStatusBar.Show(string.Format(Localization.Get("DDS UpdateProcess 2 Win"), version));
        DownloadingUpdate = false;
    }
}