﻿using UnityEngine;

class ServerPanelManager
{
    GUIServerPanel.SettingType type;
    public ServerPanelManager(GUIServerPanel.SettingType type)
    {
        this.type = type;
    }
    protected static void UpdateDataInWindow()
    {
        GUIServerPanel.PlayerStats = string.Format(Localization.Get("DDS ServerPanel WindowLabel1"), TNManager.player.name, GameDayManager.GetReadableGameTimeOfLocalPlayer());
        int i=0;
        foreach (GUIServerPanel.SettingType settingType in GUIServerPanel.SettingType.GetValues(typeof(GUIServerPanel.SettingType)))
        {
            GUIServerPanel.SetTextOfSetting(settingType, string.Format(Localization.Get("DDS ServerPanel WindowSetting " + i),GetReadableValue(GetSettedValueFromType(settingType))));
            i++;
        }
    }
    protected static string GetReadableValue(bool value)
    {
        return value ? "Disabled" : "Enabled";
    }
    protected static bool GetSettedValueFromType(GUIServerPanel.SettingType settingType)
    {
        //NGUIDebug.Log(settingType.ToString());
        switch (settingType)
        {
            case GUIServerPanel.SettingType.CommentingShipWreck:
                return MyPlayerCustomValues.DisabledCommentingShipWrecks;
            case GUIServerPanel.SettingType.CommentingMidnightAndNewDay:
                return MyPlayerCustomValues.DisabledCommentingMidnightDay;
            case GUIServerPanel.SettingType.CommeningNewWeekMonthYear:
                return MyPlayerCustomValues.DisabledCommentingWeekMonthYear;
            case GUIServerPanel.SettingType.EventsHappening:
                return MyPlayerCustomValues.DisabledEventsHappeningToPlayer;
        }
        return MyPlayerCustomValues.DisabledCaptainHastur1Quest;
    }

    public static void HandleIconClick()
    {
        GUIServerPanel.ShowWindow();
        UpdateDataInWindow();
    }
    public void HandleSettingToggle()
    {
        switch (type)
        {
            case GUIServerPanel.SettingType.CommentingShipWreck:
                MyPlayerCustomValues.DisabledCommentingShipWrecks = !MyPlayerCustomValues.DisabledCommentingShipWrecks;
                break;
            case GUIServerPanel.SettingType.CommentingMidnightAndNewDay:
                MyPlayerCustomValues.DisabledCommentingMidnightDay=!MyPlayerCustomValues.DisabledCommentingMidnightDay;
                break;
            case GUIServerPanel.SettingType.CommeningNewWeekMonthYear:
                MyPlayerCustomValues.DisabledCommentingWeekMonthYear=!MyPlayerCustomValues.DisabledCommentingWeekMonthYear;
                break;
            case GUIServerPanel.SettingType.EventsHappening:
                MyPlayerCustomValues.DisabledEventsHappeningToPlayer=!MyPlayerCustomValues.DisabledEventsHappeningToPlayer;
                break;
        }
        UpdateDataInWindow();
    }
}

