﻿using UnityEngine;
using System.Collections;
using TNet;

//funkcjonalnosci dotykajace w jakis sposob istoty servera nie gameplayu jako takiego
internal class ServerManager
{
    static List<IEnumerator> initCoroutines;
    public static List<IEnumerator> InitCoroutines { get { return initCoroutines; } }
    public static void Init()
    {
        initCoroutines = new List<IEnumerator>();
        initCoroutines.Add(MainMenuManager.UpdateCoroutine());
        initCoroutines.Add(ServerManager.UpdateEverySecond());
        initCoroutines.Add(ServerManager.UpdateEvery10Seconds());
        initCoroutines.Add(ServerManager.UpdateEveryMinute());
        GUIManager.Init(Utils.IsThisRunningOnWindows());
        //NGUIDebug.Log("this");
    }
    public static void InitializeAfterSucessfulJoin()
    {
        UIGameChat.onCommand -= GameChatExtender.HandleChatCommand;
        UIGameChat.onCommand += GameChatExtender.HandleChatCommand;
        GameChatMessagePrinter.PrintWelcomeMessage(TNManager.player.name);
    }
    public static void OnChangingRegions()
    {
        ServerAdminTools.Init();
        GUIManager.OnChangingRegions();
    }
    public static IEnumerator UpdateEveryMinute()
    {
        for (; ; )
        {
            BroadcastMaintenanceMessageIfNeeded();
            yield return new WaitForSeconds(60f);
        }
    }
    public static IEnumerator UpdateEvery10Seconds()
    {
        for (; ; )
        {
            WebsiteManager.ScheduleUpdateOfAllData();
            yield return new WaitForSeconds(10f);
        }
    }
    public static IEnumerator UpdateEverySecond()
    {
        for (; ; )
        {
            WebsiteManager.ParseAllJsons();
            ServerAdminTools.Update();
            ModUpdater.CheckIfNewVersionAppeared();
            yield return new WaitForSeconds(1f);
        }   
    }

    protected static void BroadcastMaintenanceMessageIfNeeded()
    {
        if (!WebsiteManager.GetIfServerMaintenanceIsNeeded())
            return;
        GameChatMessagePrinter.SendMaintenanceBroadcastMessage(WebsiteManager.GetServerMaintenanceMessage(), WebsiteManager.GetServerMaintenanceDateString());
    }

}

