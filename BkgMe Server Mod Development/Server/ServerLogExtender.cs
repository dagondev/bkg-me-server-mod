﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;

/**
 * Klasa odpowiada za wysylanie customowych logow do servera
 */ 
class ServerLogExtender
{
    public static IEnumerator LogAboutCreatedTown(GameTown town, int channelID)
    {
        while(UILoadingScreen.isVisible)
        {
            yield return null;
        }
        TNManager.Log("Created town " + town.id + " (" + town.name + ") in region #" + channelID + " for faction: " + town.factionID);
    }
}

