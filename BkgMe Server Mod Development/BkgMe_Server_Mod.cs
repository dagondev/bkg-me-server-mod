﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;
using DDGUI;

//glowna klasa ktora wie o innych - pomost miedzy menagerami, ale tylko gameplayowymi, sprawy stricte serwerowe anizeli gameplayowe oddaje ServerManagerowi
public class BkgMeServerMod : MonoBehaviour
{
    public const int WORLD_CREATION_CHANNEL = 10000;
    public static bool joinedAlready=false;
    public static bool isInGame = false;

    public virtual void Start()
    {
        //GameConfig.onInit -= ServerAdminTools.OnInitGameConfig;
        //GameConfig.onInit += ServerAdminTools.OnInitGameConfig;
        WebsiteManager.Init();
        //DDGUIDebug.DebugOn = false;
        ServerManager.Init();
        foreach (IEnumerator coroutine in ServerManager.InitCoroutines)
            StartCoroutine(coroutine);
        //NGUIDebug.Log("this");
    }

    public virtual void OnNetworkConnect(bool success, string msg)
    {
        MainMenuManager.update = false;
        if (!success)
            return;
        LocalizationManager.Init();
    }
    public virtual void OnNetworkDisconnect()
    { 
        //need to recreate mainmenu panel       
        StartCoroutine(RecreateMainMenu());
    }
    public static IEnumerator RecreateMainMenu()
    {
        while (DDGUIHelper.GetMainMenuGUIParentForNewElements() == null)
        {
            yield return new WaitForSeconds(0.1f);
        }
        GUIManager.Init(Utils.IsThisRunningOnWindows());
        MainMenuManager.update = true;
    }
    public virtual void OnNetworkJoinChannel(bool success, string errMsg)
    {
        if (!GameInitialized(success))
            return;
        //wyswietl na czat informacje powitalna, tylko raz
        TNManager.Log("World seed, probably at loading screen: " + GameWorld.seed);
        if (!joinedAlready)
            StartCoroutine(InitializeStuffCoroutine());
        ServerManager.OnChangingRegions();
      
        GameConfig.onGetReaction -= FactionManager.OnGetReaction;
        GameConfig.onGetReaction += FactionManager.OnGetReaction;       
    }

    protected virtual IEnumerator InitializeStuffCoroutine()
    {
        while (UILoadingScreen.isVisible)
        {
            yield return null;
        }
        TNManager.Log("World seed, definetly after loading screen: " + GameWorld.seed);
        joinedAlready = true;
        ServerManager.InitializeAfterSucessfulJoin();

        GameDayManager.Init();
        GameTown.onInit -= OnTownInit;
        GameTown.onInit += OnTownInit;
        EventsManager.Init();
        StartCoroutine(EventsManager.UpdateEveryMinuteCoroutine());
        StartCoroutine(EventsManager.UpdateEverySecondCoroutine());
        StartCoroutine(QuestManager.InitCoroutine());
        StartCoroutine(GameDayManager.UpdateCoroutine());
        StartCoroutine(SkillsManager.UpdateCoroutine());
        DDGUIDebug.PrintUIRootElementsToFile("Proper Game");
        yield return null;
    }
    protected virtual void DestroyInitializeStuffCoroutine()
    {
        joinedAlready = false;
        if(GameTown.onInit!=null)
            GameTown.onInit -= OnTownInit;
        StopCoroutine(GameDayManager.UpdateCoroutine());
        StopCoroutine(SkillsManager.UpdateCoroutine());
        StopCoroutine(QuestManager.InitCoroutine());
    }
    public static bool GameInitialized(bool success)
    {
        if (!success)
            return false;
        int channelID = TNManager.channelID;
        //jesli kanal tworzenia swiata to zignoruj
        if (channelID == WORLD_CREATION_CHANNEL)
            return false;

        return true;
    }
    protected void OnTownInit(GameTown town)
    {
        StartCoroutine(ServerLogExtender.LogAboutCreatedTown(town, TNManager.channelID));
    }

}
    