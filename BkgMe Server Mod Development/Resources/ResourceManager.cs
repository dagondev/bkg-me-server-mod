﻿using UnityEngine;
using System;


class ResourceManager
{
    public enum LootResource
    { 
		Gold,
		Wood,
		Stone,
		Powder,
        Random,
    }
    public enum TownResource
    {
        Marble= 0,
        Lumber = 1,
        Iron = 2,
        Coal = 3,
        Glass = 4,
        CraftingTools = 5,
        Crafts = 6,
        Jewellery = 7,
        FishingTools = 8,
        Meat = 9,
        Wheat = 10,
        Produce = 11,
        Spices = 12,
        Rice = 13,
        Salt = 14,
        Tobacco = 15,
        Fur = 16,
        Leather = 17,
        Wool = 18,
        Silk = 19,
        Cotton = 20,
        Incense = 21,
        Diamonds = 22,
        Coffee = 23,
        Random
    }
    public static void CreateFloatingLootWithOneResource(LootResource lootType,Vector3 position, int value)
    { 
        FloatingLoot.Create(position, new NamedInt[]{ new NamedInt(GetLootResourceName(lootType),value)});
    }
    public static void CreateFloatingLootWithSeveralResource(LootResource[] lootTypeArray, Vector3 position, int value)
    {
        NamedInt[] resources = new NamedInt[lootTypeArray.Length];
        for (int i = 0; i < lootTypeArray.Length; i++)
        {
            resources[i] = new NamedInt(GetLootResourceName(lootTypeArray[i]), value);
        }
        FloatingLoot.Create(position, resources);
    }
    public static string GetLootResourceName(LootResource lootType)
    {
        switch (lootType)
        { 
            case LootResource.Gold:
                return "gold";
            case LootResource.Wood:
                return "wood";
            case LootResource.Stone:
                return "stone";
            case LootResource.Powder:
                return "powder";
            case LootResource.Random:
                Array values = Enum.GetValues(typeof(LootResource));
                return GetLootResourceName((LootResource)values.GetValue(UnityEngine.Random.Range(0, values.Length - 1)));
            default:
                return "";
        }
    }
    public static string GetLootResourceName(int lootType)
    {
        Array values = Enum.GetValues(typeof(LootResource));
        if (lootType>values.Length)
            return GetLootResourceName(LootResource.Random);
        else
            return GetLootResourceName((LootResource)values.GetValue(lootType));
    }
    public static string GetTownResourceName(TownResource townResource)
    {
        switch (townResource)
        {
            case TownResource.Marble:
            case TownResource.Lumber:
            case TownResource.Iron:
            case TownResource.Coal:
            case TownResource.Glass:
            case TownResource.CraftingTools:
            case TownResource.Crafts:
            case TownResource.Jewellery:
            case TownResource.FishingTools:
            case TownResource.Meat:
            case TownResource.Wheat:
            case TownResource.Produce:
            case TownResource.Spices:
            case TownResource.Rice:
            case TownResource.Salt:
            case TownResource.Tobacco:
            case TownResource.Fur:
            case TownResource.Leather:
            case TownResource.Wool:
            case TownResource.Silk:
            case TownResource.Cotton:
            case TownResource.Incense:
            case TownResource.Diamonds:
            case TownResource.Coffee:
                return "Res"+(int)townResource;
            case TownResource.Random:
                Array values = Enum.GetValues(typeof(TownResource));
                return GetTownResourceName((TownResource)values.GetValue(UnityEngine.Random.Range(0, values.Length - 1)));

            default:
                return "";
        }
    }
    public static string GetTownResourceName(int resType)
    {
        Array values = Enum.GetValues(typeof(TownResource));
        if (resType > values.Length)
            return GetTownResourceName(TownResource.Random);
        else
            return GetTownResourceName((TownResource)values.GetValue(resType));
    }
    public static string GetReadableTownResourceName(string s)
    {
        switch (s)
        { 
            case "Res0":
            return "Marble";
            case "Res1":
            return "Lumber";
            case "Res2":
            return "Iron";
            case "Res3":
            return "Coal";
            case "Res4":
            return "Glass";
            case "Res5":
            return "CraftingTools";
            case "Res6":
            return "Crafts";
            case "Res7":
            return "Jewellery";
            case "Res8":
            return "FishingTools";
            case "Res9":
            return "Meat";
            case "Res10":
            return "Wheat";
            case "Res11":
            return "Produce";
            case "Res12":
            return "Spices";
            case "Res13":
            return "Rice";
            case "Res14":
            return "Salt";
            case "Res15":
            return "Tobacco";
            case "Res16":
            return "Fur";
            case "Res17":
            return "Leather";
            case "Res18":
            return "Wool";
            case "Res19":
            return "Silk";
            case "Res20":
            return "Cotton";
            case "Res21":
            return "Incense";
            case "Res22":
            return "Diamonds";
            case "Res23":
            return "Coffee";
            default:
            return "";
        }
    }
}

