﻿using UnityEngine;
using DDGUI;
using System.Collections;
using System;


class MainMenuManager : MonoBehaviour
{  
    public static bool update = true;
    
    public static IEnumerator UpdateCoroutine()
    {
        for (; ; )
        {
            yield return new WaitForSeconds(0.5f);
            if (!update)
                continue;
            string news = WebsiteManager.GetAnnouncementTime().ToLocalTime() + "\n" + WebsiteManager.GetAnnoucementText();
            GUIMainMenuRightPanel.NewsLabelText = string.Format(Localization.Get("DDS MainMenu News"), DDGUIHelper.SetColorForMessageInUILabel(news, GameChatMessagePrinter.PANEL_TEXT_COLOR));

            string playersCount = DDGUIHelper.SetColorForMessageInUILabel("0", GameChatMessagePrinter.PANEL_TEXT_COLOR);
            if (WebsiteManager.GetPlayersOnlineCount() > 0)
                playersCount = DDGUIHelper.SetColorForMessageInUILabel("" + WebsiteManager.GetPlayersOnlineCount(), GameChatMessagePrinter.PANEL_TEXT_COLOR);
            GUIMainMenuRightPanel.PlayersOnlineLabelText = string.Format(Localization.Get("DDS MainMenu Players"), playersCount);

            string maintenance = DDGUIHelper.SetColorForMessageInUILabel(Localization.Get("DDS MaintenanceMsg NotPlanned"), GameChatMessagePrinter.PANEL_TEXT_COLOR);
            if (WebsiteManager.GetIfServerMaintenanceIsNeeded())
                maintenance = DDGUIHelper.SetColorForMessageInUILabel(WebsiteManager.GetServerMaintenanceMessage() +
                    "\n At: " + WebsiteManager.GetServerMaintenanceDateString(), GameChatMessagePrinter.PANEL_TEXT_COLOR);
            GUIMainMenuRightPanel.MaintenanceLabelText = string.Format(Localization.Get("DDS MainMenu Maintenance"), maintenance);

            ulong currentVersion = ulong.Parse(Utils.GetCurrentVersion());
            ulong newestVersion = 0;
            string newestVersionString = WebsiteManager.GetNewestModVersionAsString();
            string currentVersionString = DDGUIHelper.SetColorForMessageInUILabel("" + currentVersion, GameChatMessagePrinter.PANEL_TEXT_COLOR);
            if (!newestVersionString.Equals(""))
                newestVersion = ulong.Parse(newestVersionString);

            GUIMainMenuRightPanel.UpdateToNewestButtonUpdate(newestVersion > currentVersion,(ModUpdater.DownloadingUpdate&&!ModUpdater.CheckForNewVersion));
            GUIMainMenuRightPanel.ClientVersionLabelText = string.Format(Localization.Get("DDS MainMenu ClientVersion"), currentVersionString);
            GUIMainMenuRightPanel.NewestLabelText = string.Format(Localization.Get("DDS MainMenu NewestVersion"), DDGUIHelper.SetColorForMessageInUILabel("" + newestVersionString, GameChatMessagePrinter.PANEL_TEXT_COLOR));

        }
    }
    public static void DownloadingProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
    {
        GUIMainMenuRightPanel.DownloadingProgressValue= e.ProgressPercentage;
    }
    public static EventDelegate GetEventDelegateHandlePanelClosing() { return new EventDelegate(() => update = false); }
    public static EventDelegate GetEventDelegateHandleUpdatingClient() { return new EventDelegate(() => ModUpdater.UpdateClient()); }
}

