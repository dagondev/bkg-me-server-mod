﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;
using DDGUI;


/**
 *Klasa do developmentu, pozwala sprawdzic nowe rzeczy bez obawy ze przedostana sie do glownego builda
 */ 
class BkgMeServerModDevelopment : BkgMeServerMod
{
    public new void Start()
    {
        base.Start();
        DDGUIDebug.DebugOn = true;
        DDGUIDebug.DumpAtlasesSpriteNames();
        DDGUIDebug.PrintUIRootElementsToFile("Main Menu");
        System.DateTime buildTime = Utils.RetrieveLinkerTimestamp();
        UIStatusBar.Show("BkgMe Server Mod (builded: " + buildTime.ToString() + ") Loaded - Development version");
    }
    public override void OnNetworkConnect(bool success, string msg)
    {
        if (!success)
            return;
        if (!GameInitialized(success))
            return;
        base.OnNetworkConnect(success, msg);
        TNManager.Log("Development Version HERE!");

    }
    public override void OnNetworkJoinChannel(bool success, string errMsg)
    {
        if (!GameInitialized(success))
            return;
        base.OnNetworkJoinChannel(success, errMsg);

        
    }

}

