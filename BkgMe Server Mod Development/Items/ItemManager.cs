﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//ogarnia wszystko zwiazane z itemami i PlayerItemami
class ItemManager
{
    public static string ICON_SCROLL = "Item - Scroll";
    public static string ICON_CRATES = "Item - Crates";
    public static PlayerItem GenerateLocalizedItem(string itemName, string itemInfo, string itemIcon, int gold=10)
    {
        return GenerateItem(Localization.Get(itemName), Localization.Get(itemInfo), itemIcon,gold);   
    }
    public static PlayerItem GenerateItem(string itemName, string itemInfo, string itemIcon, int gold=10)
    {
        PlayerItem playerItem2 = new PlayerItem();
        playerItem2.name = itemName;
        playerItem2.info = itemInfo;
        playerItem2.icon = itemIcon;
        playerItem2.gold = gold;
        return playerItem2;
    }

}

