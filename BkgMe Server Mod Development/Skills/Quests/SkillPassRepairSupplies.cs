﻿using UnityEngine;
using DDGUI;
using System.Collections;
using TNet;

public class SkillPassRepairSupplies : SkillImprove
{
    public SkillPassRepairSupplies()
        : base()
    { }
    public SkillPassRepairSupplies(GameUnit unit) : base()
    {
        this.mUnit = unit;
    }   
    public override bool CanUse(Vector3 pos)
    {
        bool canUse= false;

        //czy sa repair supplies
        foreach (DataNode dataNode in MyPlayer.equipment.children)
        {
            if (!dataNode.name.Contains("Cargo"))
                continue;
            if (dataNode.value == null)
                continue;
            if (dataNode.value.GetType() != typeof(PlayerItem))
                continue;
            if (((PlayerItem)dataNode.value).name.Equals(Localization.Get("QuestProtectionOfGoodsTransport Supplies Item Name")))
                canUse = true;
        }
        if (!canUse)
            return canUse;
        canUse = base.CanUse(pos);
        return canUse;
    }
    public override bool Use(GameUnit target)
    {
        QuestSupplyRepairsOfTowers quest=null;
        GuardTower tower = null;
        List<GuardTower> guardTowersToVisit = null;
        foreach(Quest qst in QuestSupplyRepairsOfTowers.list)
        {
            if (qst.GetType() != typeof(QuestSupplyRepairsOfTowers))
                continue;
            if (qst.unit != MyPlayer.ship)
                continue;
            quest = (QuestSupplyRepairsOfTowers)qst;
            guardTowersToVisit = ((QuestSupplyRepairsOfTowers)quest).GuardTowersToVisit;
            if(target.GetType()!=typeof(GuardTower))
                continue;
            if(guardTowersToVisit.Contains((GuardTower)target))
                tower=(GuardTower)target;
        }
        if (tower == null || guardTowersToVisit == null || quest == null)
            return false;
        bool used = base.Use(target);
        if (!used)
            return used;
        int left = guardTowersToVisit.Count - 1;
        foreach (DataNode dataNode in MyPlayer.equipment.children)
        {
            if (!dataNode.name.Contains("Cargo"))
                continue;
            if (dataNode.value == null)
                continue;
            if (dataNode.value.GetType() != typeof(PlayerItem))
                continue;
            if (((PlayerItem)dataNode.value).name.Equals(Localization.Get("QuestProtectionOfGoodsTransport Supplies Item Name")))
            {
                dataNode.value = null;
                UIStatusBar.ShowLocalized("QuestSupplyRepairsOfTowers SuppliesRemoved");
                break;
            }
        }
        MyPlayerManager.SayTextOverShip(string.Format(Localization.Get("QuestSupplyRepairsOfTowers Continue"), left), 2f);
        UIStatusBar.Show(string.Format(Localization.Get("QuestSupplyRepairsOfTowers Continue"), left), 2f);
        quest.IdleTime = 0;
        if (left == 0)
        {
            MyPlayerManager.SayLocalizedTextOverShip("QuestSupplyRepairsOfTowers End1", 3f);
            UIStatusBar.ShowLocalized("QuestSupplyRepairsOfTowers End1");
        }
        guardTowersToVisit.Remove(tower);
       return used;
    }
    public override NamedInt[] baseResources
    {
        get
        {
            return new NamedInt[]{};
        }
    }
    public override float baseCooldown
    {
        get
        {
            return 0.1f;
        }
    }
    public override string skillInfo
    {
        get
        {
            return Localization.Get("SkillPassRepairSupplies Name");
        }
    }
    public override string skillIcon
    {
        get
        {
            return "Skill - Build";
        }
    }
    public override string skillName
    {
        get
        {
            return Localization.Get("SkillPassRepairSupplies Info");
        }
    }
}
