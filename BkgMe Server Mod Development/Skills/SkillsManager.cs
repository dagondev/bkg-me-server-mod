﻿using UnityEngine;
using System.Collections;

class SkillsManager
{
    public static IEnumerator UpdateCoroutine()
    {
        bool saidAboutIt=false;
        for (; ; )
        {
            yield return new WaitForSeconds(0.2f);
            if (MyPlayerCustomValues.DisabledCommentingShipWrecks)
                continue;
            if (MyPlayer.ship == null)
                continue;
            SkillDivingSuit skill = MyPlayer.ship.GetComponent<SkillDivingSuit>();
            if (skill == null)
                continue;
            //UIStatusBar.Show("Enabled: " + skill.enabled + " activeCooldown: " + skill.activeCooldown + " isUsable: " + skill.isUsable);
            if (skill.isUsable)
            {
                if(saidAboutIt)
                    continue;
                int n = Random.Range(0, LocalizationManager.MaxShipWreckComment);
                MyPlayerManager.SayLocalizedTextOverShip("DDS Events ShipWreckSeen " + n);
                saidAboutIt = true;
                yield return new WaitForSeconds(1f);
            } else
            {
                saidAboutIt=false;
            }
        }
    }
}
