﻿
using UnityEngine;
//ogarnia wszystko zwiazane z playerami
class MyPlayerManager
{

    public static void SayTextOverShip(string textID)
    {
        SayTextOverShip(textID, Color.white, 2f);
    }
    public static void SayTextOverShip(string textID, float time)
    {
        SayTextOverShip(textID, Color.white, time);
    }  
    public static void SayLocalizedTextOverShip(string textID)
    {
        SayLocalizedTextOverShip(textID, Color.white, 2f);
    }
    public static void SayLocalizedTextOverShip(string textID, float time)
    {
        SayLocalizedTextOverShip(textID, Color.white, time);
    }
    /// <summary>
    /// Hepler function to preserver space.
    /// Says only if line exists! (it is important as when switching regions it likes to throw null)
    /// </summary>
    /// <param name="textID"></param>
    public static void SayLocalizedTextOverShip(string textID, Color color, float time)
    {
        if(MyPlayer.ship==null)
            return;
        if (MyPlayer.ship.hud == null)
            return;
        if (Localization.Exists(textID))
            MyPlayer.ship.AddHudText(textID, color, time, true);
    }
    public static void SayTextOverShip(string textID, Color color, float time)
    {
        if (MyPlayer.ship == null)
            return;
        if (MyPlayer.ship.hud == null)
            return;
        MyPlayer.ship.AddHudText(textID, color, time,false);
    }
    public static void SayLocalizedTextOverShip(string textID, Color color, float time,GameUnit unit)
    {
        if (unit == null)
            return;
        if (unit.hud == null)
            return;
        if (Localization.Exists(textID))
            unit.AddHudText(textID, color, time,true);
    }
    public static void HandleFactionSwitch()
    {
        if(!MyPlayerCustomValues.SelectedSecondFaction)
            return;
        if (MyPlayer.ship.isInCombat)
        {
            UIStatusBar.Show(Localization.Get("DDS FactionChange InCombat"));
            return;
        }
        string faction = FactionManager.GetName(MyPlayerManager.GetSecondFactionIDOfLocalPlayer());
        int startRegion = 0;
        switch (faction)
        {
            case "Valiant":
                MyPlayer.factionID = 1;
                startRegion = 44;
                break;
            case "Exchange":
                MyPlayer.factionID = 4;
                startRegion = 78;
                break;
            case "Sojourn":
                MyPlayer.factionID = 3;
                startRegion = 14;
                break;
            case "Consulate":
                MyPlayer.factionID = 2;
                startRegion = 10;
                break;

        }
        MyPlayer.saveNeeded = true;
        GameChat.SendGlobal("Changing faction to " + faction + ", moving to starting region #" + startRegion);
        MyPlayer.TravelTo(startRegion);
    }
    public static int GetSecondFactionIDOfLocalPlayer()
    {
        int currentFaction = MyPlayer.factionID;
        int firstFaction = MyPlayerCustomValues.FirstFactionID;
        int secondFaction = MyPlayerCustomValues.SecondFactionID;
        if (currentFaction == firstFaction)
            return secondFaction;
        else
            return firstFaction;
    }
    public static void SetSecondFaction(int secondFactionID)
    {
        MyPlayerCustomValues.SelectedSecondFaction = true;
        MyPlayerCustomValues.FirstFactionID=MyPlayer.factionID;
        MyPlayerCustomValues.SecondFactionID=secondFactionID;
    }

}

