﻿using System;
using System.Collections.Generic;


class MyPlayerCustomValues
{
    public static bool DisabledCommentingShipWrecks
    {
        get { return GetValueFromLocalPlayer<bool>("disabledCommentingShipWrecks"); }
        set { SetValueToLocalPlayer("disabledCommentingShipWrecks", value); }
    }
    public static bool DisabledCommentingMidnightDay
    {
        get { return GetValueFromLocalPlayer<bool>("disabledCommentingMidnightDay"); }
        set { SetValueToLocalPlayer("disabledCommentingMidnightDay", value); }
    }
    public static bool DisabledCommentingWeekMonthYear
    {
        get { return GetValueFromLocalPlayer<bool>("disabledCommentingWeekMonthYear"); }
        set { SetValueToLocalPlayer("disabledCommentingWeekMonthYear", value); }
    }
    public static bool DisabledEventsHappeningToPlayer
    {
        get { return GetValueFromLocalPlayer<bool>("disabledEventsHappeningToPlayer"); }
        set { SetValueToLocalPlayer("disabledEventsHappeningToPlayer", value); }
    }
    public static bool DisabledLighthouse1Quest
    {
        get { return GetValueFromLocalPlayer<bool>("disabledLighthouse1Quest"); }
        set { SetValueToLocalPlayer("disabledLighthouse1Quest", value); }
    }
    public static bool DisabledCaptainHastur1Quest
    {
        get { return GetValueFromLocalPlayer<bool>("disabledCaptainHastur1Quest"); }
        set { SetValueToLocalPlayer("disabledCaptainHastur1Quest", value); }
    }
    public static DateTime StartedPlayingDate
    {
        get { return GetValueFromLocalPlayer<DateTime>("startDate"); }
        set { SetValueToLocalPlayer("startDate",value); }        
    }
    public static long DaysPassedInGame
    {
        get { return GetValueFromLocalPlayer<long>("daysPassedInGame"); }
        set { SetValueToLocalPlayer("daysPassedInGame", value); }
    }
    public static bool SelectedSecondFaction 
    {
        get { return GetValueFromLocalPlayer<bool>("choosedSecondFaction"); }
        set { SetValueToLocalPlayer("choosedSecondFaction", value); }
    }
    public static int FirstFactionID 
    {
        get { return GetValueFromLocalPlayer<int>("firstFactionID"); }
        set { SetValueToLocalPlayer("firstFactionID", value); }     
    }
    public static int SecondFactionID 
    {
        get { return GetValueFromLocalPlayer<int>("secondFactionID"); }
        set { SetValueToLocalPlayer("secondFactionID", value); }  
    }    
    public static bool WasSeenOnServer 
    {
        get { return GetValueFromLocalPlayer<bool>("wasSeenOnServer"); }
        set { SetValueToLocalPlayer("wasSeenOnServer", value); }
    }
    public static bool HasturQuest
    {
        get { return GetValueFromLocalPlayer<bool>("hasturQuest"); }
        set { SetValueToLocalPlayer("hasturQuest", value); }
    }
    public static bool DoneSupplyTowersQuest
    {
        get { return GetValueFromLocalPlayer<bool>("doneSupplyTowersQuest"); }
        set { SetValueToLocalPlayer("doneSupplyTowersQuest", value); }
    }

    public static void ResetCustomValues()
    {
        
    }
    protected static void SetValueToLocalPlayer(string key, object value)
    {
        string keyString = key;
        if (keyString == "")
            return;
        MyPlayer.Set(keyString, value);
        MyPlayer.saveNeeded = true;
    }
    protected static T GetValueFromLocalPlayer<T>(string key)
    {
        string keyString = key;
        if (keyString == "")
            return default(T);
        return MyPlayer.Get<T>(keyString);
    }
}

