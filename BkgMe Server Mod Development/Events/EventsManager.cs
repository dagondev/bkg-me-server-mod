﻿using DDGUI;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections;


public class EventsManager : MonoBehaviour
{
    public static void Init()
    {
        Random.seed = (int)System.DateTime.Now.TimeOfDay.Ticks;
    }
    public static IEnumerator UpdateEveryMinuteCoroutine()
    {
        for (; ; )
        {
            yield return new WaitForSeconds(60f);
            if (MyPlayerCustomValues.DisabledEventsHappeningToPlayer)
                continue;
            if (!MyPlayerCustomValues.WasSeenOnServer)
            {
                continue;
            }
            bool hasturQuest = MyPlayerCustomValues.HasturQuest;
            float chanceForKnownEvent = 0.01f;
            float chanceForUnknownEvent = 0.03f;
            if (Random.Range(0f, 1f) <= chanceForKnownEvent)
            {
                 
                UIQuestAlert.ShowLocalizedQuestAlert("DDS Events Start", "DDS Events StatusText1");
                yield return new WaitForSeconds(2f);
                KnownEvents.FireRandomKnownEvent();
            }
            else if (Random.Range(0f, 1f) <= chanceForUnknownEvent)
            {
                GameTown town = GameTown.Find("Glenham");
                if (town == null)
                    continue;
                bool hasSpices = false;
                foreach (PlayerItem item in town.inventory)
                { 
                    if(item.name.Equals(Localization.Get("GlenhamSpices Item Name"))) 
                    {
                        hasSpices=true;
                        break;
                    }

                }
                if(!hasSpices)
                    town.inventory.Add(ItemManager.GenerateLocalizedItem("GlenhamSpices Item Name","GlenhamSpices Item Info",ItemManager.ICON_SCROLL,2000));
            }
           
        }
    }
    public static IEnumerator UpdateEverySecondCoroutine()
    {
        for (; ; )
        {
            yield return new WaitForSeconds(1f);
        }
    }

}

