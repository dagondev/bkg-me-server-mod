﻿using DDGUI;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections;

class KnownEvents
{
    public static bool broadcastMessage = false;
    delegate void GameEvent();
    private static Collection<GameEvent> allGameEvents;
    private static Collection<GameEvent> goodGameEvents;
    private static Collection<GameEvent> badGameEvents;
    private static Collection<GameEvent> neutralGameEvents;
    static Collection<GameEvent> GoodGameEvents
    {
        get
        {
            if (goodGameEvents != null)
                return goodGameEvents;
            goodGameEvents = new Collection<GameEvent>();
            goodGameEvents.Add(Generate10QuestForEachTownEvent);
            goodGameEvents.Add(TwiceCurrentlyFloatingLootEvent);
            goodGameEvents.Add(GenerateSupplyOfRandomResourceInAllTownsEvent);
            goodGameEvents.Add(GenerateShortageOfRandomResourceInAllTownsEvent);
            goodGameEvents.Add(GeneratePlayerFactionFleet);
            return goodGameEvents;
        }
    }
    static Collection<GameEvent> NeutralGameEvents
    {
        get
        {
            if (neutralGameEvents != null)
                return neutralGameEvents;
            neutralGameEvents = new Collection<GameEvent>();
            neutralGameEvents.Add(ResetAllTownsEvent);
            return neutralGameEvents;
        }
    }
    static Collection<GameEvent> BadGameEvents
    {
        get
        {
            if (badGameEvents != null)
                return badGameEvents;
            badGameEvents = new Collection<GameEvent>();
            badGameEvents.Add(GeneratePirateFleetEvent);
            badGameEvents.Add(GeneratePirateLordWithFleetEvent);
            return badGameEvents;
        }
    }
    public static void FireRandomKnownEvent()
    {
        if (!MyPlayerCustomValues.WasSeenOnServer)
            return;
        bool hasturQuest = MyPlayerCustomValues.HasturQuest;
        float chanceForGoodEvent = 0.65f;
        float chanceForNeutralEvent = 0.8f;
        float diceRoll = Random.Range(0f, 1f);
        if (diceRoll <= chanceForGoodEvent)
        {
            GoodGameEvents[Random.Range(0, GoodGameEvents.Count - 1)].Invoke();
            return;
        }
        else if (diceRoll <= chanceForNeutralEvent)
        {
            NeutralGameEvents[Random.Range(0, NeutralGameEvents.Count - 1)].Invoke();
            return;
        }
        else
        {
            BadGameEvents[Random.Range(0, BadGameEvents.Count - 1)].Invoke();
        }
        return;
    }
    public static void Generate10QuestForEachTownEvent()
    {
        foreach (GameTown town in GameTown.list)
        {
            town.AddQuests(10);
        }
        UIQuestAlert.ShowLocalizedQuestAlert(Localization.Get("DDS Events Event1"), "");
        if (broadcastMessage)
        {
            GameChat.SendGlobal(DDGUIHelper.SetColorForMessageInUILabel("Event: All towns in the region #" + MyPlayer.zoneID + " got fresh quests!", GameChatMessagePrinter.SERVER_SUCCESS_COLOR));
            broadcastMessage = false;
        }
    }
    public static void TwiceCurrentlyFloatingLootEvent()
    {
        int count = FloatingLoot.list.Count;
        for (int i = 0; i < count; i++)
        {
            Vector3 pos = new Vector3();
            float angle = 1;
            pos = FloatingLoot.list[i].position;
            float chance = Random.Range(0f, 1f);
            if (chance < 0.2f)
                ResourceManager.CreateFloatingLootWithOneResource(ResourceManager.LootResource.Random, pos, Random.Range(10, 100));
            else if (chance < 0.4f)
                ResourceManager.CreateFloatingLootWithSeveralResource(new ResourceManager.LootResource[] { ResourceManager.LootResource.Random, ResourceManager.LootResource.Random }, pos, Random.Range(10, 100));
            else if (chance < 0.6f)
                FloatingLoot.Create(pos, new NamedInt[] { new NamedInt("gold", Random.Range(10, 100)), new NamedInt("wood", Random.Range(10, 100)) });
            else if (chance < 0.8f)
                FloatingLoot.Create(pos, GameConfig.GenerateRandomItem(GameZone.challengeLevel, false, Random.Range(1, 4), null));
            else if (chance > 0.9f)
                FloatingLoot.Create(pos, GameConfig.GenerateRandomQuest(null));
        }
        UIQuestAlert.ShowLocalizedQuestAlert(Localization.Get("DDS Events Event2"), "");

        if (broadcastMessage)
        {
            GameChat.SendGlobal(DDGUIHelper.SetColorForMessageInUILabel("Event: Suddenly, in the region #" + MyPlayer.zoneID + " new floating Loot appeared!", GameChatMessagePrinter.SERVER_SUCCESS_COLOR));
            broadcastMessage = false;
        }
    }
    public static void ResetAllTownsEvent()
    {

        float chance = Random.Range(0f, 1f);
        foreach (GameTown town in GameTown.list)
        {
            town.ResetEverything();
        }
        UIQuestAlert.ShowLocalizedQuestAlert(Localization.Get("DDS Events Event3"), "");
        if (broadcastMessage)
        {
            GameChat.SendGlobal(DDGUIHelper.SetColorForMessageInUILabel("Event: All towns in the region #" + MyPlayer.zoneID + " got reseted!", GameChatMessagePrinter.SERVER_SUCCESS_COLOR));
            broadcastMessage = false;
        }
    }
    public static void GenerateSupplyOfRandomResourceInAllTownsEvent()
    {
        float chance = Random.Range(0f, 1f);
        string res = ResourceManager.GetTownResourceName(ResourceManager.TownResource.Random);
        foreach (GameTown town in GameTown.list)
        {
            town.SetProduction(res, 4);
        }
        UIQuestAlert.ShowLocalizedQuestAlert(Localization.Get("DDS Events Event4"), "");
        if (broadcastMessage)
        {
            GameChat.SendGlobal(DDGUIHelper.SetColorForMessageInUILabel(string.Format("Event: All towns in the region #" + MyPlayer.zoneID + " got supply of {0}!", ResourceManager.GetReadableTownResourceName(res)), GameChatMessagePrinter.SERVER_SUCCESS_COLOR));
            broadcastMessage = false;
        }
    }
    public static void GenerateShortageOfRandomResourceInAllTownsEvent()
    {
        float chance = Random.Range(0f, 1f);
        string res = ResourceManager.GetTownResourceName(ResourceManager.TownResource.Random);
        foreach (GameTown town in GameTown.list)
        {
            town.SetDemand(res, 4);
        }
        UIQuestAlert.ShowLocalizedQuestAlert(Localization.Get("DDS Events Event5"), "");
        if (broadcastMessage)
        {
            GameChat.SendGlobal(DDGUIHelper.SetColorForMessageInUILabel(string.Format("Event: All towns in the region #" + MyPlayer.zoneID + " have shortage of {0}!", ResourceManager.GetReadableTownResourceName(res)), GameChatMessagePrinter.SERVER_SUCCESS_COLOR));
            broadcastMessage = false;
        }
    }
    public static void GeneratePlayerFactionFleet()
    {
        Vector3 pos = new Vector3();
        float angle = 0;
        GameShip.GetRandomSpawnPosition(out pos, out angle);
        for (int i = 0; i < 5; i++)
        {
            GameShip.CreateRandom(string.Format("{0} Fleet Ship", FactionManager.GetName(MyPlayer.factionID)), pos, angle, MyPlayer.factionID, 0, 1, 1, 0);
        }
        UIQuestAlert.ShowLocalizedQuestAlert(Localization.Get("DDS Events Event6"), "");
        if (broadcastMessage)
        {
            GameChat.SendGlobal(DDGUIHelper.SetColorForMessageInUILabel(string.Format("Event: {0} fleet appeared in the region #" + MyPlayer.zoneID + "", FactionManager.GetName(MyPlayer.factionID)), GameChatMessagePrinter.SERVER_SUCCESS_COLOR));
            broadcastMessage = false;
        }
    }
    public static void GeneratePirateFleetEvent()
    {
        Vector3 pos = new Vector3();
        float angle = 0;
        GameShip.GetRandomSpawnPosition(out pos, out angle);
        for (int i = 0; i < 5; i++)
        {
            GameShip.CreateRandom("Pirate Fleet Ship", pos, angle, 0, 0, 1, 1, 0);
        }
        UIQuestAlert.ShowLocalizedQuestAlert(Localization.Get("DDS Events Event7"), "");
        if (broadcastMessage)
        {
            GameChat.SendGlobal(DDGUIHelper.SetColorForMessageInUILabel("Event: Pirate fleet attacked the region #" + MyPlayer.zoneID + ". We should defeat them before they lay eggs!", GameChatMessagePrinter.SERVER_SUCCESS_COLOR));
            broadcastMessage = false;
        }
    }
    public static void GeneratePirateLordWithFleetEvent()
    {
        Vector3 pos = new Vector3();
        float angle = 0;
        for (int i = 0; i < 5; i++)
        {
            GameShip.GetRandomSpawnPosition(out pos, out angle);
            GameShip.CreateRandom("Pirate Fleet Ship", pos, angle, 0, 0, 1, 1, 0);
        }
        GameShip.CreateRandom("Pirate Lord", pos, angle, 0, 1, 1, 1, 1);
        UIQuestAlert.ShowLocalizedQuestAlert(Localization.Get("DDS Events Event8"), "");
        if (broadcastMessage)
        {
            GameChat.SendGlobal(DDGUIHelper.SetColorForMessageInUILabel("Event: Pirate Lord with his fleet attacked region #" + MyPlayer.zoneID + ". Swim away, while you can!", GameChatMessagePrinter.SERVER_SUCCESS_COLOR));
            broadcastMessage = false;
        }
    }
}
