﻿using System.Collections;
using UnityEngine;

class GameDayManager
{
    static long days;
    static long weeks;
    static long months;
    static long years;
    static long hours;
    static long minutes;
    static float lastTime;
    static bool lastIsItNight;
    static int randomNewDayComment;

    public delegate void onMidnight();
    public delegate void onNewDay(long daysPassedOverall);
    public delegate void onNewWeek(long weeksPassedOverall);
    public delegate void onNewMonth(long monthsPassedOverall);
    public delegate void onNewYear(long yearsPassedOverall);
    public static onMidnight OnMidnight;
    public static onNewDay OnNewDay;
    public static onNewWeek OnNewWeek;
    public static onNewMonth OnNewMonth;
    public static onNewYear OnNewYear;
    public static void Init()
    {
        days = MyPlayerCustomValues.DaysPassedInGame;
        if (days == 0)
        {
            MyPlayerCustomValues.StartedPlayingDate = System.DateTime.Now;
        }
        weeks = days/7;
        months = days/31;
        years = days/365;
        float time = GameSun.time * 24;
        hours = (int)time; 
        minutes = (int)((time - hours) * 100);
        lastTime = GameSun.time;
        lastIsItNight = IsItNightTime();
        OnNewDay = new onNewDay(OnNewDayEvent);
        OnMidnight = new onMidnight(OnMidnightEvent);
    }

    protected static void OnMidnightEvent()
    {
        //TODO: przeniesc te komentarze wszystkie do osobnej klasy?
        randomNewDayComment = Random.Range(0, LocalizationManager.MaxNewDayComment);
        //zwykle jak sie zmienia region to rzuca nullem :|
        if(!MyPlayerCustomValues.DisabledCommentingMidnightDay)
            MyPlayerManager.SayLocalizedTextOverShip("DDS Events MidnightRemark " + randomNewDayComment, 3f);
    }
    protected static void OnNewDayEvent(long daysPassedOverall)
    {
        int n = randomNewDayComment;
        int typeOfMessage = 0;
        days++;
        MyPlayerCustomValues.DaysPassedInGame = days;
        if (days % 7 == 0)
        {
            typeOfMessage = 1;
            n = Random.Range(0, LocalizationManager.MaxNewWeekComment);
            weeks++;
            if (OnNewWeek != null)
                OnNewWeek(weeks);
        }
        if (days % 31 == 0)
        {
            typeOfMessage = 2;
            n = Random.Range(0, LocalizationManager.MaxNewMonthComment);
            months++;
            if (OnNewMonth != null)
                OnNewMonth(months);
        }
        if (days % 365 == 0)
        {
            typeOfMessage = 3;
            n = Random.Range(0, LocalizationManager.MaxNewYearComment);
            years++;
            if (OnNewYear != null)
                OnNewYear(years);
        }
        switch(typeOfMessage)
        {
            case 0:
                if(!MyPlayerCustomValues.DisabledCommentingMidnightDay)
                    MyPlayerManager.SayLocalizedTextOverShip("DDS Events NewDayRelief " + n,3f);
                break;
            case 1:
                if (!MyPlayerCustomValues.DisabledCommentingWeekMonthYear)
                    MyPlayerManager.SayLocalizedTextOverShip("DDS Events NewWeekComment " + n,3f);
                break;
            case 2:
                if (!MyPlayerCustomValues.DisabledCommentingWeekMonthYear)
                    MyPlayerManager.SayLocalizedTextOverShip("DDS Events NewMonthComment " + n,3f);
                break;
            case 3:
                if (!MyPlayerCustomValues.DisabledCommentingWeekMonthYear)
                    MyPlayerManager.SayLocalizedTextOverShip("DDS Events NewYearComment " + n, 3f);
                break;
        }
        
    }
    public static string GetReadableGameTimeOfLocalPlayer()
    {
        return years + " years, " + months%12 + " months, " + weeks%4 + " weeks, " + days%7 + " days.";
    }
    public static string GetReadableTime()
    {
        return hours+"";
    }
    public static bool IsItNightTime()
    {
        return GameSun.isNightTime;
    }
    public static IEnumerator UpdateCoroutine()
    {
        for (; ; )
        {
            float time =GameSun.time*24;
            hours = (int)time;
            minutes = (int)((time - hours)*100);
            if (IsItNightTime()&&!lastIsItNight)
               OnMidnight();
            if (!IsItNightTime()&&lastIsItNight)
                OnNewDay(days);
            lastTime = time;
            lastIsItNight = IsItNightTime();
            yield return new WaitForSeconds(0.1f);
        }
    }
}

