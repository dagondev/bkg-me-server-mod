﻿using UnityEngine;
using DDGUI;
using System.Collections;
using System.Collections.Generic;

public class QuestTransportGoodsToLighthouse : Quest
{
    bool arrived = false;
    bool talked1 = false;
    //odpowiada za dodanie questa do miasta jesli jest jakikolwiek isolated lighthouse
    public static List<GameUnit> PlayersDoingQuest;
    public static List<GameUnit> isolatedLighthousesThatGotStock;

    public virtual void OnNetworkJoinChannel(bool success, string errMsg)
    {
        if (!BkgMeServerMod.GameInitialized(success))
            return;
        PlayersDoingQuest = new List<GameUnit>();
        isolatedLighthousesThatGotStock = new List<GameUnit>();
        MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse ChangedRegion", 5f);
        Complete(false);
    }
    //odpowiada za sprawdzenie czy dla odizolowanych lighthousow minal juz tydzien i mozna przydzielic nowe quest

    protected IEnumerator CheckConditions()
    {
         
        for (; ; )
        {
            yield return new WaitForSeconds(2f);
            if (mUnit != MyPlayer.ship)
                continue;
            if (mTargetUnit == null)
                continue;
            if (mTargetUnit.isPlayerClose && mTargetUnit.InProximityTo(mUnit) && mTargetUnit.weaponsRangeList.Contains(mUnit))
            {
                if (!arrived)
                {
                    arrived = true;
                    MyPlayer.ship.ForceBrake();
                    continue;
                }
                else
                {
                    if (!talked1)
                    {
                        MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse Encounter1", 2f);
                        talked1 = true;
                        continue;
                    }
                    else
                    {
                        int randomResponse = Random.Range(0, LocalizationManager.MaxQuestTransportGoodsToLighthouseComment);
                        MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse Encounter2 " + randomResponse, Color.white, 1.5f, mTargetUnit);
                        MyPlayer.ship.ForceBrake();
                        yield return new WaitForSeconds(2f);
                        MyPlayer.ship.ForceBrake();
                        MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse Encounter3", 1.5f);
                        MyPlayer.ship.ForceBrake();
                        yield return new WaitForSeconds(2f);
                        MyPlayer.ship.ForceBrake();
                        MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse Encounter4 " + randomResponse, Color.white, 1.5f, mTargetUnit);
                        yield return new WaitForSeconds(2f);
                        if (randomResponse > 0)
                            MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse Encounter5", 1.5f);
                        Complete(true);
                        break;
                    }
                }

            }
            else
            {
                if (arrived)
                    MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse TooFar", 2f);
                arrived = false;
                talked1 = false;
                continue;
            }

        }
    }
    protected override void OnComplete(bool success)
    {
        base.OnComplete(success);
        if (mTargetUnit == null||mUnit!=MyPlayer.ship)
            return;
        if (PlayersDoingQuest.Contains(mUnit))
            PlayersDoingQuest.Remove(mUnit);
        if(MyPlayer.HasItem(Localization.Get("QuestTransportGoodsToLighthouse Item Name")))
            MyPlayer.RemoveItem(MyPlayer.FindShipmentItem(Localization.Get("QuestTransportGoodsToLighthouse Item Name")));
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        arrived = false;
        talked1 = false;
        if (PlayersDoingQuest == null)
            PlayersDoingQuest = new List<GameUnit>();
        if (mUnit == MyPlayer.ship)
        {
            if (Lighthouse.list == null||Lighthouse.list.Count == 0)
            {
                MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse ChangedRegion NoIsolatedOne", Color.white, 5f);
                Complete(false);
                return;
            }
            if (PlayersDoingQuest.Contains(mUnit))
            {
                MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse Duplicated", Color.white, 5f);
                Complete(false);
                return;
            }
            foreach (GameUnit lighthouse in Lighthouse.list)
            {
                bool towerIsNotAlone = false;
                if (GameTown.list == null)
                {
                    Complete(false);
                    return;                    
                }
                foreach (GameTown gameTown in GameTown.list)
                {
                    if (gameTown == null)
                        continue;
                    if (lighthouse == null)
                        continue;
                    if (gameTown.InProximityTo(lighthouse))
                    {
                        towerIsNotAlone = true;
                        break;
                    }
                    
                }
                if (towerIsNotAlone)
                    continue;               
                if (lighthouse.GetType() != typeof(Lighthouse))
                    continue;
                //if (isolatedLighthousesThatGotStock.Contains(lighthouse))
                //    continue;
                mTargetUnit = lighthouse;
                targetUnit = lighthouse;
                break;
            }
            //if (mTargetUnit == null)
            //{
            //    if (isolatedLighthousesThatGotStock == null)
            //        isolatedLighthousesThatGotStock = new List<GameUnit>();
            //    if (isolatedLighthousesThatGotStock.Count > 0)
            //        mTargetUnit = isolatedLighthousesThatGotStock[Random.Range(0, isolatedLighthousesThatGotStock.Count - 1)];
            //}
            if (mTargetUnit == null)
            {
                if (PlayersDoingQuest == null)
                    PlayersDoingQuest = new List<GameUnit>();
                Complete(false);
                if (PlayersDoingQuest.Contains(mUnit))
                    PlayersDoingQuest.Remove(mUnit);
                MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse ChangedRegion NoIsolatedOne", Color.white, 5f);
                return;
            } 
            else
            {
                if (PlayersDoingQuest == null)
                    PlayersDoingQuest = new List<GameUnit>();
                MyPlayerManager.SayLocalizedTextOverShip("QuestTransportGoodsToLighthouse Marked", Color.white, 5f);
                PlayersDoingQuest.Add(mUnit);
                StartCoroutine(CheckConditions());
            }

        }
    }
}

