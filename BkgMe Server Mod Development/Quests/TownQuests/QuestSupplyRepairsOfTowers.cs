﻿using UnityEngine;
using DDGUI;
using System.Collections;
using TNet;

class QuestSupplyRepairsOfTowers : Quest
{
    public List<GuardTower> GuardTowersToVisit = new List<GuardTower>();
    static List<GameUnit> playersDoingQuests=new List<GameUnit>();
    int numberOfSuppliesSaved;
    public float IdleTime;
    protected override void OnDisable()
    {
        base.OnDisable();
        //writ of service
        if (MyPlayer.HasItem(Localization.Get("QuestProtectionOfGoodsTransport Item Name")))
            MyPlayer.RemoveItem(MyPlayer.FindShipmentItem(Localization.Get("QuestProtectionOfGoodsTransport Item Name")));
        playersDoingQuests.Remove(mUnit);
        foreach (DataNode dataNode in MyPlayer.equipment.children)
        {
            if (!dataNode.name.Contains("Cargo"))
                continue;
            if (dataNode.value == null)
                continue;
            if (dataNode.value.GetType() != typeof(PlayerItem))
                continue;
            if (((PlayerItem)dataNode.value).name.Equals(Localization.Get("QuestProtectionOfGoodsTransport Supplies Item Name")))
            {
                dataNode.value = null;
            }
        }
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        if (mUnit != MyPlayer.ship)
            return;

        foreach (GameUnit unit in GuardTower.list)
        {
            if (unit.GetType() == typeof(GuardTower))
                if (GameConfig.GetReaction(MyPlayer.ship, unit) != GameConfig.Reaction.Hostile)
                    GuardTowersToVisit.Add((GuardTower)unit);
        }
        if (GuardTowersToVisit.Count == 0)
        {
            MyPlayerManager.SayLocalizedTextOverShip("QuestSupplyRepairsOfTowers NoTower");
            Complete(false);
            return;
        }
        if (playersDoingQuests.Contains(mUnit))
        {
            MyPlayerManager.SayLocalizedTextOverShip("QuestSupplyRepairsOfTowers Duplicated");
            Complete(false);
            return;
        }
        if (!CanDoThisQuest())
        {
            Complete(false);
            return;
        }
        
        numberOfSuppliesSaved = GuardTowersToVisit.Count*2;
        playersDoingQuests.Add(mUnit);
        MyPlayerManager.SayTextOverShip(string.Format(Localization.Get("QuestSupplyRepairsOfTowers Start"),GuardTowersToVisit.Count));
        StartCoroutine(CheckConditions());
    }

    protected IEnumerator CheckConditions()
    {
        for (; ; )
        {
            IdleTime += 2f;
            //jesli gracz przez ostatnie 2 minuty niczego nie dostarczyl to wskaz mu wieze
            if (IdleTime >= 120)
            {
                IdleTime = 0;
                if (GuardTowersToVisit.Count > 0)
                    mTargetUnit = GuardTowersToVisit[0];
                if (mTargetUnit != null)
                {
                    MyPlayerManager.SayTextOverShip(string.Format(Localization.Get("QuestSupplyRepairsOfTowers Lost"), GameTown.FindClosest(mTargetUnit.position).name));
                    UIStatusBar.Show(string.Format(Localization.Get("QuestSupplyRepairsOfTowers Lost"), GameTown.FindClosest(mTargetUnit.position).name));
                }
            }
            yield return new WaitForSeconds(2f);
            if (mUnit != MyPlayer.ship)
                continue;
            if (MyPlayer.isDocked && GuardTowersToVisit.Count > 0)
            {               
                TNet.DataNode emptyCargoSlot = MyPlayer.FindEmptyCargoSlot();
                if (emptyCargoSlot != null)
                {
                    emptyCargoSlot.value = ItemManager.GenerateLocalizedItem("QuestProtectionOfGoodsTransport Supplies Item Name","QuestProtectionOfGoodsTransport Supplies Item Info",ItemManager.ICON_CRATES);
                    UIStatusBar.ShowLocalized("QuestSupplyRepairsOfTowers SuppliesLoaded");
                    continue;
                }
               
            }
            //sprawdzmy czy player jest przy jakiejs wiezy
            foreach (GuardTower tower in GuardTowersToVisit)
            {
                SkillPassRepairSupplies skill = mUnit.GetComponent<SkillPassRepairSupplies>();
                
                if (skill == null)
                    break;
                if(skill.CanUse(tower.position))
                {
                    if (GameConfig.GetReaction(MyPlayer.ship, unit) == GameConfig.Reaction.Hostile)
                    {
                        UIStatusBar.ShowLocalized("QuestSupplyRepairsOfTowers WrongFaction");
                    }
                    else
                    {
                        MyPlayerManager.SayLocalizedTextOverShip("QuestSupplyRepairsOfTowers Encounter1",1f);
                        yield return new WaitForSeconds(1f);
                        int commentID = Random.Range(0, LocalizationManager.MaxQuestSupplyRepairsOfTowersComment);
                        MyPlayerManager.SayLocalizedTextOverShip("QuestSupplyRepairsOfTowers Encounter2 " + commentID, Color.white,1f,tower);
                        IdleTime = 0;
                        yield return new WaitForSeconds(5f);
                    }
                    break;
                }
            }
            //jesli oblecial wszystkie wieze to fajnie
            if (GuardTowersToVisit.Count == 0)
            {
                if (!MyPlayer.isDocked)
                    continue;
                if (MyPlayer.dockedTown.name.Equals(source))
                {
                    Complete(true);
                }
            }
        }
    }
    protected override void OnComplete(bool success)
    {
        base.OnComplete(success);
        foreach (DataNode dataNode in MyPlayer.equipment.children)
        {
            if (!dataNode.name.Contains("Cargo"))
                continue;
            if (dataNode.value == null)
                continue;
            if (dataNode.value.GetType() != typeof(PlayerItem))
                continue;
            if (((PlayerItem)dataNode.value).name.Equals(Localization.Get("QuestProtectionOfGoodsTransport Supplies Item Name")))
            {
                numberOfSuppliesSaved++;
                dataNode.value = null;
            }
        }
        if (mUnit != MyPlayer.ship)
            return;
        if (playersDoingQuests.Contains(mUnit))
            playersDoingQuests.Remove(mUnit);
        if (!success)
            return;
        //usun writ of service
        if (MyPlayer.HasItem(Localization.Get("QuestProtectionOfGoodsTransport Item Name")))
            MyPlayer.RemoveItem(MyPlayer.FindShipmentItem(Localization.Get("QuestProtectionOfGoodsTransport Item Name")));
        if (numberOfSuppliesSaved <= 0)
        {
            MyPlayerManager.SayLocalizedTextOverShip("QuestSupplyRepairsOfTowers End2 2");
        }
        else
        {
            MyPlayerCustomValues.DoneSupplyTowersQuest = true;
            int gold = numberOfSuppliesSaved * (int)(10 + 2.2f * GameZone.challengeLevel);
            MyPlayerManager.SayTextOverShip(string.Format(Localization.Get("QuestSupplyRepairsOfTowers End2 1"), gold), 2f);
            MyPlayer.AddResources(new NamedInt[] { new NamedInt("gold", gold) });
        }
    }
    protected bool CanDoThisQuest()
    {
        if (GuardTowersToVisit.Count == 0)
        {
            MyPlayerManager.SayLocalizedTextOverShip("QuestSupplyRepairsOfTowers NoTower");
            return false;
        }
        if (playersDoingQuests.Contains(mUnit))
        {
            MyPlayerManager.SayLocalizedTextOverShip("QuestSupplyRepairsOfTowers Duplicated");
            return false;
        }
        return true;
    }
}

