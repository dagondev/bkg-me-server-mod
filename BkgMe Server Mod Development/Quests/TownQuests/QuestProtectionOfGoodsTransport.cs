﻿using UnityEngine;
using DDGUI;

public class QuestProtectionOfGoodsTransport : Quest
{
    GameShip trader;
    void OnCollisionEnter(Collision coll)
    {
        if (mUnit == trader)
        {
            if (coll.collider.GetComponent<GameDock>() != null)
            {
                UIStatusBar.Show("w doku!");
            }
            if (coll.collider.GetComponent<GameTown>() != null)
            {
                UIStatusBar.Show("w miescie!");
            }
        }
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        if (mUnit != MyPlayer.ship)
            return;
        GameTown targetTown = GameTown.FindRandom(MyPlayer.factionID);
        GameShip.Create("Trader", "Ship6", MyPlayer.factionID, GameTown.FindClosest(MyPlayer.ship.position).respawnPos, GameTown.FindClosest(MyPlayer.ship.position).respawnAngle, 0, 1f, 0f, 0);
        GameShip trader;
        MyPlayer.target = targetTown;
        foreach (GameShip ship in GameShip.list)
        {            
            if (ship.name == "Trader")
            {
                trader = ship;
                trader.GetComponent<AIShip>().enabled = false;
                targetUnit = trader;
                mUnit = trader;
                if (Random.Range(0f, 1f) <= 0.5)
                {
                    trader.GetComponent<AIShip>().NavigateTo(targetTown.position);
                    trader.GetComponent<AIShip>().state = AIShip.State.Forced;
                    trader.AddHudText("I don`t have time for this!",Color.white,10f);

                    MyPlayer.ship.AddHudText("You foul! Wait for us!", Color.white, 10f);
                    MyPlayer.ShowMessage(string.Format("This fool is sailing straight to {0}!", targetTown.name));
                }
                else
                {
                    MyPlayer.ship.AddHudText("Follow us, we will get you nice and smooth to {0}.", Color.white, 10f);
                    trader.AddHudText("Following you, Captain!", Color.white, 10f);
                    
                    trader.GetComponent<AIShip>().FollowTarget(MyPlayer.ship, false);
                }
                break;
            }
        }
        PlayerItem writ = MyPlayer.FindShipmentItem("Writ of Protection Service");
        if (writ != null)
        {
            writ.info = string.Format(Localization.Get("QuestProtectionOfGoodsTransport Item Info"), targetTown.name);
        }
            
        
    }

    protected override void OnComplete(bool success)
    {
        if (success && MyPlayer.ship == mUnit)
        {

        }
    }
}

