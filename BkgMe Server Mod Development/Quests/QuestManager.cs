﻿using UnityEngine;
using Tasharen;
using System.Collections;
using TNet;


//ogarnia wszystko zwiazane z questami
class QuestManager
{
    
    public static IEnumerator InitCoroutine()
    {
        if (!MyPlayerCustomValues.WasSeenOnServer)
        {
            yield return new WaitForSeconds(5f);
            MyPlayerCustomValues.WasSeenOnServer = true;
            MyPlayerCustomValues.HasturQuest = true;
            MyPlayer.AwardXP(1000);
            UIQuestAlert.ShowLocalizedQuestAlert("DDS WelcomeMessage Desc", "DDS WelcomeMessage Title", "DDS WelcomeMessage Title");
            PlayerItem reward = ItemManager.GenerateLocalizedItem("DDS WelcomeMessage Item Name", "DDS WelcomeMessage Item Info", ItemManager.ICON_CRATES);
            reward.gold = 2000;
            reward.SetSalvage("gold", 2000);
            MyPlayer.AddItem(reward);
        }
    }
    public static IEnumerator UpdateCoroutine()
    {
        for (; ; )
        {
            yield return new WaitForSeconds(1f);
        }
    }

    public static PlayerItem GenerateLocalizedQuestItem(string questName, string itemName, string itemInfo)
    {
        return GenerateQuestItem(questName, Localization.Get(itemName), Localization.Get(itemInfo));
    }
    public static PlayerItem GenerateQuestItem(string questName, string itemName, string itemInfo)
    {
        PlayerItem playerItem2 = ItemManager.GenerateItem(itemName, itemInfo, ItemManager.ICON_SCROLL);
        playerItem2.quest = new DataNode("Quest", questName);
        return playerItem2;
    }

}

